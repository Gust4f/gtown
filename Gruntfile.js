module.exports = function(grunt) {
    "use strict";
    grunt.initConfig({
        clean: {
            css: ['src/main/resources/static/assets/css/*.css'],
            js: ['src/main/resources/static/assets/js/*.js', 'src/main/resources/static/assets/js/*.map']
        },
        uglify: {
            gtown: {
                options: {
                    expand: false,
                    flatten: false,
                    banner: '/*! <%= grunt.template.today("dd-mm-yyyy") %> */\n',
                    sourceMap: true,
                    sourceMapIncludeSources: true,
                    screwIE8: true
                },
                files: {
                    'src/main/resources/static/assets/js/login.min.js': 'src/main/javascript/login.js',
                    'src/main/resources/static/assets/js/gtown.min.js': [
                        'src/main/javascript/footer.js',
                        'src/main/javascript/gtown.js'
                    ]
                }
            },
            customlibs: {
                options: {
                    expand: false,
                    flatten: false,
                    sourceMap: false,
                    screwIE8: true
                },
                files: {
                    'src/main/resources/static/assets/js/lib/bootstrap.min.js': [
                        'node_modules/bootstrap/js/alert.js',
                        'node_modules/bootstrap/js/collapse.js',
                        'node_modules/bootstrap/js/modal.js',
                        'node_modules/bootstrap/js/transition.js',
                        'node_modules/bootstrap/js/button.js'
                    ]
                }
            }
        },
        jasmine: {
            src: [
                'src/main/javascript/gtown.js'
            ],
            options: {
                specs: [
                    'src/test/javascript/me/**/*.js'
                ],
                vendor: [
                    'src/main/resources/static/assets/js/lib/jquery.min.js',
                    'node_modules/sammy/lib/sammy.js'
                ],
                helpers: [
                    'src/test/javascript/lib/mock-ajax.js'
                ],
                junit: {
                    path: 'build/test-results/'
                },
                template: require('grunt-template-jasmine-istanbul'),
                templateOptions: {
                    coverage: 'build/reports/javascript/coverage/coverage.json',
                    report: 'build/reports/javascript/coverage',
                    thresholds: {
                        lines: 0,
                        statements: 0,
                        branches: 0,
                        functions: 0
                    }
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'src/main/javascript/**/*.js', 'src/test/javascript/**/*.js'],
            options: {
                globals: {
                    jquery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        less: {
            default: {
                options: {
                    compress: true,
                    cleancss: true
                },
                files: {
                    'src/main/resources/static/assets/css/error.min.css': 'src/main/less/error.less',
                    'src/main/resources/static/assets/css/login.min.css': 'src/main/less/login.less',
                    'src/main/resources/static/assets/css/bootstrap.min.css': 'src/main/less/bootstrap.less',
                    'src/main/resources/static/assets/css/animate.min.css': 'src/main/less/animate.less'
                }
            }
        },
        sass: {
            default: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'src/main/resources/static/assets/css/gtown.min.css': 'src/main/sass/gtown.scss'
                }
            }
        },
        copy: {
            default: {
                files: [
                    {
                        src: ['node_modules/bootstrap/dist/fonts/*'],
                        dest: 'src/main/resources/static/assets/fonts/',
                        expand: true,
                        flatten: true
                    },
                    {
                        src: [
                            'node_modules/jasmine-ajax/lib/*',
                            'node_modules/jquery/dist/jquery.min.*',
                            'node_modules/sammy/lib/min/sammy-latest.min.*',
                            'node_modules/flexslider/jquery.flexslider-min.*',
                            'node_modules/fancybox/dist/js/jquery.fancybox.pack.js',
                            'node_modules/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.min.*',
                            'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.*'
                        ],
                        dest: 'src/main/resources/static/assets/js/lib/',
                        expand: true,
                        flatten: true
                    },
                    {
                        src: [
                            'node_modules/fancybox/dist/css/jquery.fancybox.css',
                            'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.*'
                        ],
                        dest: 'src/main/resources/static/assets/css/',
                        expand: true,
                        flatten: true
                    },
                    {
                        src: ['node_modules/fancybox/dist/img/*'],
                        dest: 'src/main/resources/static/assets/img/',
                        expand: true,
                        flatten: true
                    }
                ]
            }
        },
        watch: {
            jshint: {
                files: ['<%= jshint.files %>'],
                tasks: ['jshint', 'jasmine']
            },
            javascript: {
                files: ['src/main/javascript/*.js'],
                tasks: ['uglify']
            },
            less: {
                files: ['src/main/less/*.less'],
                tasks: ['less']
            },
            sass: {
                files: ['src/main/sass/*.sass'],
                tasks: ['scss']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('test', ['jshint', 'jasmine']);

    grunt.registerTask('snapshot', ['jshint', 'copy:default', 'uglify:customlibs', 'uglify:gtown', 'less:default', 'sass:default']);
    grunt.registerTask('default', ['snapshot']);
};
