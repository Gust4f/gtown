package me.report;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public final class JVMGCReport extends JVMReport {

    public JVMGCReport() {
        super(new GarbageCollectorMetricSet());
    }

    public Map<String, Object> getGCDataMap() {
        final Map<String, Object> values = new ConcurrentHashMap<>();
        values.put("jvm_garbage_marksweep_count", getValue("PS-MarkSweep.count"));
        values.put("jvm_garbage_marksweep_time", getValue("PS-MarkSweep.time"));
        values.put("jvm_garbage_scavenge_count", getValue("PS-Scavenge.count"));
        values.put("jvm_garbage_scavenge_time", getValue("PS-Scavenge.time"));
        return Collections.unmodifiableMap(values);
    }

    private long getValue(final String gauge) {
        final Gauge metric = (Gauge) metrics.get(gauge);
        if (metric == null || metric.getValue() == null) {
            return 0;
        }
        return (long) metric.getValue();
    }
}
