package me.report;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static me.util.BootstrapProgressbarStatus.*;

@Component
public final class JVMMemoryReport extends JVMReport {

    public JVMMemoryReport() {
        super(new MemoryUsageGaugeSet());
    }

    public Map<String, Object> getJVMMemoryUsageMap() {
        final Map<String, Object> values = new ConcurrentHashMap<>();
        values.put("jvm_memory_total_used", getTotalMB("total.used"));
        values.put("jvm_memory_total_max", getTotalMB("total.max"));
        final long totalProgress = getTotalProgress("total.used", "total.max");
        values.put("jvm_memory_total_progress", totalProgress);
        values.put("jvm_memory_total_status", getStatus(totalProgress));
        return Collections.unmodifiableMap(values);
    }

    public Map<String, Object> getJVMHeapDataMap() {
        final Map<String, Object> values = new ConcurrentHashMap<>();
        values.put("jvm_memory_heap_used", getTotalMB("heap.used"));
        values.put("jvm_memory_heap_max", getTotalMB("heap.max"));
        final long totalProgress = getTotalProgress("heap.used", "heap.max");
        values.put("jvm_memory_heap_progress", totalProgress);
        values.put("jvm_memory_heap_status", getStatus(totalProgress));
        return Collections.unmodifiableMap(values);
    }

    public Map<String, Object> getJVMNonHeapDataMap() {
        final Map<String, Object> values = new ConcurrentHashMap<>();
        values.put("jvm_memory_non_heap_used", getTotalMB("non-heap.used"));
        values.put("jvm_memory_non_heap_committed", getTotalMB("non-heap.committed"));
        final long totalProgress = getTotalProgress("non-heap.used", "non-heap.committed");
        values.put("jvm_memory_non_heap_progress", totalProgress);
        values.put("jvm_memory_non_heap_status", getStatus(totalProgress));
        return Collections.unmodifiableMap(values);
    }

    private long getTotalProgress(final String gauge1, final String gauge2) {
        final Gauge metric1 = (Gauge) metrics.get(gauge1);
        final Gauge metric2 = (Gauge) metrics.get(gauge2);

        final Double value1 = new Double((long) metric1.getValue());
        final Double value2 = new Double((long) metric2.getValue());

        final Double result = (value1 / value2) * 100;

        return result.longValue();
    }

    private long getTotalMB(final String gauge) {
        final Gauge metric = (Gauge) metrics.get(gauge);
        if (metric == null || metric.getValue() == null) {
            return 0;
        }
        final long value = (long) metric.getValue();
        return value / 1000000;
    }

    private String getStatus(final long load) {
        if (load < 50) {
            return OK;
        } else if (load < 80) {
            return WARNING;
        }
        return DANGER;
    }
}
