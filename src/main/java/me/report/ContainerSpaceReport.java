package me.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public final class ContainerSpaceReport {

    private final static Logger LOGGER = LoggerFactory.getLogger(ContainerSpaceReport.class);

    public Map<String, Object> getContainerSpaceDataMap() {
        Map<String, Object> values = new ConcurrentHashMap<>();
        File file = createTempFile();
        values.put("container_free_space", file == null ? 0 : getTotalMB(file.getUsableSpace()));
        values.put("container_total_space", file == null ? 0 : getTotalMB(file.getTotalSpace()));
        values.put("container_space_progress", file == null ? 0 : getTotalProgress(file.getUsableSpace(), file.getTotalSpace()));
        return values;
    }

    private File createTempFile() {
        try {
            return File.createTempFile("tmp", "txt");
        } catch (Exception e) {
            LOGGER.warn("Could not create temporary file to determine disk space due to: {}", e);
        }
        return null;
    }

    private long getTotalProgress(final long free, final long total) {
        final Double value1 = new Double(free);
        final Double value2 = new Double(total);

        final Double result = (value1 / value2) * 100;

        return result.longValue();
    }

    private long getTotalMB(final long value) {
        return value / 1000000;
    }
}
