package me.report;

import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricSet;

import java.util.Map;

class JVMReport {

    private final MetricSet metricSet;

    protected JVMReport(final MetricSet metricSet) {
        this.metricSet = metricSet;
    }

    protected Map<String, Metric> metrics;

    public final synchronized Map<String, Metric> measure() {
        metrics = metricSet.getMetrics();
        return metrics;
    }
}
