package me.report;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public final class JVMThreadsReport extends JVMReport {

    public JVMThreadsReport() {
        super(new ThreadStatesGaugeSet());
    }

    public Map<String, Object> getJVMThreadsDataMap() {
        final Map<String, Object> values = new ConcurrentHashMap<>();
        values.put("jvm_threads_count", getCount("count"));
        values.put("jvm_threads_runnable_count", getCount("runnable.count"));
        values.put("jvm_threads_progress", getTotalProgress("runnable.count", "count"));

        values.put("jvm_threads_timed_waiting_count", getCount("timed_waiting.count"));
        values.put("jvm_threads_time_waiting_progress", getTotalProgress("timed_waiting.count", "count"));

        values.put("jvm_threads_waiting_count", getCount("waiting.count"));
        values.put("jvm_threads_waiting_progress", getTotalProgress("waiting.count", "count"));

        values.put("jvm_threads_blocked_count", getCount("blocked.count"));
        values.put("jvm_threads_blocked_progress", getTotalProgress("blocked.count", "count"));

        return Collections.unmodifiableMap(values);
    }

    private long getCount(final String gauge) {
        final Gauge metric = (Gauge) metrics.get(gauge);
        if (metric == null || metric.getValue() == null) {
            return 0;
        }
        return (int) metric.getValue();
    }

    private long getTotalProgress(final String gauge1, final String gauge2) {
        final long metric1 = getCount(gauge1);
        final long metric2 = getCount(gauge2);

        final Double value1 = new Double(metric1);
        final Double value2 = new Double(metric2);

        final Double result = (value1 / value2) * 100;

        return result.longValue();
    }
}
