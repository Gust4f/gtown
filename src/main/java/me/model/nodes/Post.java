package me.model.nodes;

import me.model.DatedEntity;
import org.eclipse.jetty.util.ConcurrentHashSet;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Comparator;
import java.util.Date;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;

@NodeEntity(label = "Post")
public class Post implements DatedEntity {

    @GraphId
    private Long id;
    private Date date;

    @Relationship(type = "WROTE", direction = Relationship.INCOMING)
    private User user;
    private String header;
    private String body;
    private String footer;
    private String url;

    @Relationship(type = "HAS_COMMENT", direction = Relationship.OUTGOING)
    private Set<Comment> comments;

    public Post() {
        comments = new ConcurrentSkipListSet<>((t, t1) -> t.getDate().before(t1.getDate()) ? -1 : 1);
        date = new Date();
    }

    /*
        SETTERS
     */
    public void setHeader(String header) {
        this.header = header;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /*
        GETTERS
     */
    public String getUrl() {
        return url;
    }

    public Long getId() {
        return id;
    }

    public String getHeader() {
        return header;
    }

    public String getBody() {
        return body;
    }

    public String getFooter() {
        return footer;
    }

    public String getSummary() {
        return body.substring(0,250) + "...";
    }

    public User getUser() {
        return user;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Date getDate() {
        return date;
    }

    public void addComment(final Comment comment) {
        comments.add(comment);
    }
}
