package me.model.nodes;


import me.model.DatedEntity;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Date;

@NodeEntity(label = "Comment")
public class Comment implements DatedEntity {

    @GraphId
    private Long id;

    private String body;
    private Date date;

    @Relationship(type = "WROTE", direction = Relationship.INCOMING)
    private User user;

    @Relationship(type = "HAS_COMMENT", direction = Relationship.INCOMING)
    private Post post;

    public Comment(){}

    public Long getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
