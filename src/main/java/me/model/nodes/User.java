package me.model.nodes;

import org.eclipse.jetty.util.ConcurrentHashSet;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

@NodeEntity(label = "User")
public class User {

    @GraphId
    private Long id;

    private String username;

    @Relationship(type = "WROTE", direction = Relationship.OUTGOING)
    private Set<Post> posts;

    @Relationship(type = "WROTE", direction = Relationship.OUTGOING)
    private Set<Comment> comments;

    @Relationship(type = "UPLOADED", direction = Relationship.OUTGOING)
    private Set<Picture> pictures;

    public User() {
        posts = new ConcurrentSkipListSet<>((t, t1) -> t.getDate().before(t1.getDate()) ? -1 : 1);
        comments = new ConcurrentSkipListSet<>((t, t1) -> t.getDate().before(t1.getDate()) ? -1 : 1);
        pictures = new ConcurrentSkipListSet<>((t, t1) -> t.getDate().before(t1.getDate()) ? -1 : 1);
    }

    public User(final String username) {
        this();
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User wrotePost(Post post) {
        this.posts.add(post);
        return this;
    }

    public User wroteComment(Comment comment) {
        this.comments.add(comment);
        return this;
    }

    public User uploadedPicture(Picture picture) {
        this.pictures.add(picture);
        return this;
    }

    public Set<Picture> getPictures() {
        return pictures;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public Set<Comment> getComments() {
        return comments;
    }
}
