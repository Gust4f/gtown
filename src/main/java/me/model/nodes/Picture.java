package me.model.nodes;

import me.model.DatedEntity;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Date;

/*
    SHOULD NOT store images in Neo4j. Store URL to location instead.
 */
@NodeEntity
public class Picture implements DatedEntity {

    @GraphId
    private Long id;
    private Date date;

    @Relationship(type = "UPLOADED", direction = Relationship.INCOMING)
    private User user;
    private String description;
    private String name;
    private String url;
    private String contentType;

    public Picture() {
        date = new Date();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
