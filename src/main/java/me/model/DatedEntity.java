package me.model;

import java.util.Date;

public interface DatedEntity {

    void setDate(Date date);
    Date getDate();
}
