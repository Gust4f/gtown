package me.service;

import com.stormpath.sdk.account.Account;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
public interface AuthenticationCheckService {

    String getUsername(final HttpServletRequest request);
    Optional<Account> getAccount(final HttpServletRequest request);
    boolean isAuthenticated(final HttpServletRequest request);

}
