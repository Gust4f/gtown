package me.service;

import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface ReportService {

    Map<String, Object> getReportMap();
}
