package me.service.impl;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.servlet.account.AccountResolver;
import me.service.AuthenticationCheckService;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Component
class AuthenticationCheckServiceImpl implements AuthenticationCheckService {
    private final static String GUEST = "Guest";

    @Override
    public String getUsername(final HttpServletRequest request) {
        final Optional<Account> account = getAccount(request);
        return account.map(Account::getUsername).orElse(GUEST);
    }

    @Override
    public Optional<Account> getAccount(final HttpServletRequest request) {
        if (isValidAccountAttribute(request)) {
            return Optional.of(AccountResolver.INSTANCE.getRequiredAccount(request));
        }
        return Optional.empty();
    }

    private boolean isValidAccountAttribute(final HttpServletRequest request) {
        return request.getAttribute(Account.class.getName()) != null && AccountResolver.INSTANCE.hasAccount(request);
    }

    @Override
    public boolean isAuthenticated(final HttpServletRequest request) {
        return !getUsername(request).equals(GUEST);
    }
}
