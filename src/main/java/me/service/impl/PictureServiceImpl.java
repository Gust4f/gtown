package me.service.impl;

import me.dao.neo4j.GustafsNeo4jOperations;
import me.model.nodes.Picture;
import me.model.nodes.User;
import me.service.PictureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Component
class PictureServiceImpl implements PictureService {

    private final static Logger LOGGER = LoggerFactory.getLogger(PictureServiceImpl.class);

    private final GustafsNeo4jOperations neo4jOperations;

    @Value("${data.image.store.url}")
    private String dataUrl;

    @Autowired
    PictureServiceImpl(final GustafsNeo4jOperations pictureRepository) {
        this.neo4jOperations = pictureRepository;
    }

    @Override
    public boolean savePicture(final MultipartFile file, final String description, final String username) {
        final Picture picture = new Picture();
        final String name = file.getOriginalFilename().replace(' ', '_');
        final String url = dataUrl + name;
        picture.setDescription(description);
        picture.setUrl(url);
        picture.setName(name);
        picture.setContentType(file.getContentType());

        final User uploader = neo4jOperations.loadByProperty(User.class, "username", username).map(user -> {
            user.uploadedPicture(picture);
            return user;
        }).orElse(new User(username).uploadedPicture(picture));
        picture.setUser(uploader);
        neo4jOperations.save(uploader);

        try {
            file.transferTo(new File(url));
        } catch (Exception e) {
            LOGGER.error("Exception thrown when trying to store picture {}", e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public Optional<Picture> getPicture(final String name) {
        return neo4jOperations.loadByProperty(Picture.class, "name", name);
    }

    @Override
    public long getPictureCount() {
        return neo4jOperations.count(Picture.class);
    }

    @Override
    public List<Picture> getPictures() {
        final List<Picture> pictures = new ArrayList<>(neo4jOperations.loadAll(Picture.class));
        Collections.reverse(pictures);
        return Collections.unmodifiableList(pictures);
    }
}
