package me.service.impl;

import me.service.MailSendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.MailHealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Date;

import static me.util.EmailAddressValidator.isValidEmailAddress;

@Component
class MailSendServiceImpl implements MailSendService {

    private final static Logger LOGGER = LoggerFactory.getLogger(MailSendServiceImpl.class);

    private final JavaMailSenderImpl mailSender;

    @Autowired
    MailSendServiceImpl(final JavaMailSenderImpl mailSender) {
        this.mailSender = mailSender;
    }

    public boolean sendMessage(final String name, final String from, final String check, final String body) {
        if (isValidMessage(name, from, check, body)) {
            return send(name, from, body);
        }
        return false;
    }

    private boolean send(final String name, final String from, final String body) {
        final SimpleMailMessage message = buildMessage(name, from, body);
        try {
            mailSender.send(message);
        } catch (Exception e) {
            LOGGER.error("Exception was thrown when trying to send message: {}", e.getMessage());
            return false;
        }
        return true;
    }

    private SimpleMailMessage buildMessage(final String name, final String from, final String body) {
        final SimpleMailMessage message = new SimpleMailMessage();
        message.setReplyTo(from);
        message.setSentDate(new Date());
        message.setTo("gustaf@protonmail.ch");
        message.setFrom(from);
        message.setText("From " + from + "\n\n" + body);
        message.setSubject("Contact form from " + name);
        return message;
    }

    private boolean isValidMessage(final String name, final String email, final String check, final String body) {
        return isValidName(name) && isValidEmailAddress(email) && check.isEmpty() && isValidBody(body);
    }

    private boolean isValidName(final String name) {
        return !name.trim().isEmpty();
    }

    private boolean isValidBody(final String body) {
        return !body.trim().isEmpty() && body.length() > 0;
    }
}
