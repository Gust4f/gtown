package me.service.impl;

import me.report.ContainerSpaceReport;
import me.report.JVMGCReport;
import me.report.JVMMemoryReport;
import me.report.JVMThreadsReport;
import me.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
class ReportServiceImpl implements ReportService {

    private final JVMMemoryReport jvmMemoryReport;
    private final JVMThreadsReport jvmThreadsReport;
    private final JVMGCReport jvmGcReport;
    private final ContainerSpaceReport containerSpaceReport;

    @Autowired
    ReportServiceImpl(final JVMMemoryReport jvmMemoryReport,
                             final JVMThreadsReport jvmThreadsReport,
                             final JVMGCReport jvmGcReport,
                             final ContainerSpaceReport containerSpaceReport) {
        this.jvmMemoryReport = jvmMemoryReport;
        this.jvmThreadsReport = jvmThreadsReport;
        this.jvmGcReport = jvmGcReport;
        this.containerSpaceReport = containerSpaceReport;
    }

    @Override
    @Cacheable("metrics")
    public Map<String, Object> getReportMap() {
        jvmMemoryReport.measure();
        jvmThreadsReport.measure();
        jvmGcReport.measure();
        return getMap();
    }

    private Map<String, Object> getMap() {
        final Map<String, Object> reportMap = new ConcurrentHashMap<>();
        reportMap.putAll(jvmMemoryReport.getJVMMemoryUsageMap());
        reportMap.putAll(jvmMemoryReport.getJVMHeapDataMap());
        reportMap.putAll(jvmMemoryReport.getJVMNonHeapDataMap());
        reportMap.putAll(jvmThreadsReport.getJVMThreadsDataMap());

        reportMap.putAll(jvmGcReport.getGCDataMap());
        reportMap.putAll(containerSpaceReport.getContainerSpaceDataMap());
        return Collections.unmodifiableMap(reportMap);
    }
}
