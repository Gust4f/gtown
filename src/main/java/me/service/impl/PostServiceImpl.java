package me.service.impl;

import com.stormpath.sdk.account.Account;
import me.dao.neo4j.GustafsNeo4jOperations;
import me.model.nodes.Comment;
import me.model.nodes.Post;
import me.model.nodes.User;
import me.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
class PostServiceImpl implements PostService {

    private final GustafsNeo4jOperations gustafsNeo4jOperations;

    @Autowired
    PostServiceImpl(final GustafsNeo4jOperations gustafsNeo4jOperations) {
        this.gustafsNeo4jOperations = gustafsNeo4jOperations;
    }

    @Override
    public int getPostCount() {
        return (int) gustafsNeo4jOperations.count(Post.class);
    }

    @Override
    public boolean exists(final Post post) {
        return gustafsNeo4jOperations.load(Post.class, post.getId()).map(c -> true).orElse(false);
    }

    @Override
    public boolean exists(final String postId) {
        return gustafsNeo4jOperations.load(Post.class, Long.valueOf(postId)).map(c -> true).orElse(false);
    }

    @Override
    public Optional<Post> getPost(final long page) {
        return gustafsNeo4jOperations.load(Post.class, page);
    }

    @Override
    public Map<String, Object> getPostDataFromPageNr(Integer page) {
        final Map<String,Object> attributes = new HashMap<>();
        final List<Date> postDates = new ArrayList<>();
        final Iterator<Post> postIterator = getPosts().iterator();
        int current = 1;
        while (current < page) {
            postDates.add(postIterator.next().getDate());
            current++;
        }
        final Post post = postIterator.next();
        if (!postIterator.hasNext()) {
            postDates.add(post.getDate());
        } else {
            while(postIterator.hasNext()) {
                postDates.add(postIterator.next().getDate());
            }
        }
        attributes.put("post", post);
        attributes.put("postDates", postDates);
        attributes.put("lastDate", postDates.get(postDates.size()-1));
        return attributes;
    }

    private List<Post> getPosts() {
        final String query =
                "MATCH (n:Post) " +
                        "WITH n " +
                        "ORDER BY n.id DESC " +
                        "MATCH p=(q)-[:WROTE]->(n)-[*0..1]->(m)<-[*0..1]-(r) " +
                        "RETURN p";
        return gustafsNeo4jOperations.loadAllSortedByDate(Post.class, query, Collections.EMPTY_MAP);
    }

    @Override
    public List<Post> getLatestFivePosts() {
        final String query =
                "MATCH (n:Post) RETURN n";
        final List<Post> postList = gustafsNeo4jOperations.loadAllSortedByDate(Post.class, query, Collections.EMPTY_MAP);
        return postList.subList(postList.size()-5, postList.size());
    }

    @Override
    public synchronized void updatePost(final String postId, final String header, final String body) {
        gustafsNeo4jOperations.load(Post.class, Long.valueOf(postId)).ifPresent(post -> {
            post.setHeader(header);
            post.setBody(body);
            gustafsNeo4jOperations.save(post);
        });
    }

    @Override
    public void deletePost(final String postId) {
        gustafsNeo4jOperations.load(Post.class, Long.valueOf(postId)).ifPresent(post -> {
            post.getComments().forEach(comment -> gustafsNeo4jOperations.delete(comment));
            gustafsNeo4jOperations.delete(post);
        });
    }

    @Override
    public void insertPost(final String username, final String header, final String body) {
        final Post post = new Post();
        post.setHeader(header);
        post.setBody(body);

        final User author = gustafsNeo4jOperations.loadByProperty(User.class, "username", username).map(user -> {
            user.wrotePost(post);
            return user;
        }).orElse(new User(username).wrotePost(post));

        post.setUser(author);
        gustafsNeo4jOperations.save(author);
    }

    @Override
    public synchronized void createCommentOnPost(final String username, final String body, final String postId) {
        final Comment comment = new Comment();
        comment.setDate(new Date());
        comment.setBody(body);

        final User author = gustafsNeo4jOperations.loadByProperty(User.class, "username", username).map(user -> {
            user.wroteComment(comment);
            return user;
        }).orElse(new User(username).wroteComment(comment));

        comment.setUser(author);
        saveCommentOnPost(comment, postId);
    }

    private void saveCommentOnPost(final Comment comment, final String postId) {
        gustafsNeo4jOperations.load(Post.class, Long.valueOf(postId)).ifPresent(post -> {
            comment.setPost(post);
            gustafsNeo4jOperations.save(comment);
        });
    }

    @Override
    public synchronized void deleteCommentOnPost(final Account account, final String commentId, final String postId) {
        gustafsNeo4jOperations.load(Post.class, Long.valueOf(postId)).ifPresent(post -> {
            gustafsNeo4jOperations.load(Comment.class, Long.valueOf(commentId)).ifPresent(comment -> {
                if (comment.getUser().getUsername().equals(account.getUsername())) {
                    gustafsNeo4jOperations.delete(comment);
                }
            });
        });
    }
}
