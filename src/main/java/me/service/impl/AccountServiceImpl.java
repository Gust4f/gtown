package me.service.impl;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.application.Application;
import com.stormpath.sdk.authc.UsernamePasswordRequestBuilder;
import com.stormpath.sdk.client.Client;
import com.stormpath.sdk.impl.authc.DefaultUsernamePasswordRequestBuilder;
import com.stormpath.sdk.resource.ResourceException;
import me.dao.neo4j.GustafsNeo4jOperations;
import me.model.nodes.User;
import me.service.AccountService;
import me.util.EmailAddressValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static me.util.EmailAddressValidator.isValidEmailAddress;

@Component
class AccountServiceImpl implements AccountService {

    private final static Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

    private final Application application;
    private final Client client;

    @Autowired
    private final GustafsNeo4jOperations gustafsNeo4jOperations;

    @Autowired
    AccountServiceImpl(final Application application,
                              final GustafsNeo4jOperations gustafsNeo4jOperations,
                              final Client client) {
        this.application = application;
        this.gustafsNeo4jOperations = gustafsNeo4jOperations;
        this.client = client;
    }

    @Override
    public boolean saveAccount(final String login,
                               final String givenName,
                               final String surName,
                               final String userName,
                               final String email,
                               final String currentPassword,
                               final String newPassword,
                               final String rnewPassowrd) {
        if (currentPassword == null || currentPassword.trim().isEmpty()) {
            return false;
        }
        try {
            return getAccount(login, currentPassword).map(account -> {
                account.setGivenName(givenName);
                account.setSurname(surName);
                if (isValidUsername(userName)) {
                    account.setUsername(userName);
                }
                if (isValidEmailAddress(email)) {
                    account.setEmail(email);
                }
                if (!newPassword.trim().isEmpty() && newPassword.equals(rnewPassowrd)) {
                    account.setPassword(newPassword);
                }
                account.save();
                updateUserNode(login, account.getUsername());
                return true;
            }).orElse(false);
        } catch (Exception e) {
            LOGGER.error("Exception was thrown when trying to update account: {}", e.getMessage());
            return false;
        }
    }

    private void updateUserNode(final String oldUsername, final String newUsername) {
        gustafsNeo4jOperations.loadByProperty(User.class, "username", oldUsername).ifPresent(user -> {
            user.setUsername(newUsername);
            gustafsNeo4jOperations.save(user);
        });
    }

    private Optional<Account> getAccount(final String login, final String password) {
        final UsernamePasswordRequestBuilder authBuilder = new DefaultUsernamePasswordRequestBuilder();
        authBuilder.setUsernameOrEmail(login).setPassword(password);
        try {
            return Optional.of(application.authenticateAccount(authBuilder.build()).getAccount());
        } catch (ResourceException e) {
            LOGGER.error("Exception was thrown when trying to verify account: {}", e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public String sendResetPasswordEmail(final String email) {
        try {
            application.sendPasswordResetEmail(email);
            return "Email was sent to the address you entered.";
        } catch(ResourceException e) {
            if (e.getCode() == 2016) {
                return "No account linked to the supplied email found.";
            }
            return "Invalid email.";
        }
    }

    @Override
    public boolean removeAccount(final String login, final String password) {
        return getAccount(login, password).map(account -> {
            removeUserNode(account);
            account.delete();
            return true;
        }).orElse(false);
    }

    private void removeUserNode(final Account account) {
        gustafsNeo4jOperations.loadByProperty(User.class, "username", account.getUsername()).ifPresent(user -> {
            user.getComments().forEach(comment -> gustafsNeo4jOperations.delete(comment));
            user.getPosts().forEach(post -> gustafsNeo4jOperations.delete(post));
            user.getPictures().forEach(picture -> gustafsNeo4jOperations.delete(picture));
            gustafsNeo4jOperations.delete(user);
        });
    }

    @Override
    public Map<String, String> createAccount(final String givenName,
                                             final String surname,
                                             final String username,
                                             final String email,
                                             final String password) {
        final Map<String, String> messages = new ConcurrentHashMap<>();
        messages.put("success", "A confirmation email was sent to the email you supplied.");
        return verifyInput(givenName, surname, username, email, password).map(status -> {
            messages.clear();
            messages.put("problem", status);
            return messages;
        }).orElse(messages);
    }

    private Optional<String> verifyInput(final String givenName,
                                         final String surname,
                                         final String username,
                                         final String email,
                                         final String password) {
        if (!isValidGivenName(givenName)) {
            return Optional.of("Invalid First Name.");
        } else if (!isValidSurname(surname)) {
            return Optional.of("Invalid Last Name.");
        } else if (!isValidUsername(username)) {
            return Optional.of("Invalid Username.");
        } else if (accountExists(username)) {
            return Optional.of("Account already exists.");
        } else if (accountWithEmailExists(email)) {
            return Optional.of("Email already in use.");
        } else if (!isValidEmail(email)) {
            return Optional.of("Invalid email.");
        } else if (!isPasswordValid(password)) {
            return Optional.of("Password minimum length 8, 1 digit, 1 upper and 1 lower case letter.");
        }
        create(givenName, surname, username, email, password);
        return Optional.empty();
    }

    private boolean isValidGivenName(final String givenName) {
        return givenName != null && !givenName.trim().isEmpty();
    }

    private boolean isValidSurname(final String surname) {
        return surname != null && !surname.trim().isEmpty();
    }

    private boolean isValidUsername(final String username) {
        return username != null && !username.trim().isEmpty();
    }

    private boolean isValidEmail(final String email) {
        return EmailAddressValidator.isValidEmailAddress(email);
    }

    private boolean accountExists(final String username) {
        for (Account account : application.getAccounts()) {
            if (account.getUsername().equalsIgnoreCase(username)) {
                return true;
            }
        }
        return false;
    }

    private boolean accountWithEmailExists(final String email) {
        for (Account account : application.getAccounts()) {
            if (account.getEmail().equalsIgnoreCase(email)) {
                return true;
            }
        }
        return false;
    }

    /*
        (?=.*[0-9]) a digit must occur at least once
        (?=.*[a-z]) a lower case letter must occur at least once
        (?=.*[A-Z]) an upper case letter must occur at least once
        (?=.*[@#$%^&+=]) a special character must occur at least once
        (?=\\S+$) no whitespace allowed in the entire string
        .{8,} at least 8 characters
     */
    private boolean isPasswordValid(final String password) {
        final String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
        return password.matches(pattern);
    }

    private void create(final String givenName,
                        final String surname,
                        final String username,
                        final String email,
                        final String password) {
        final Account account = client.instantiate(Account.class);
        account.setGivenName(givenName);
        account.setSurname(surname);
        account.setUsername(username);
        account.setEmail(email);
        account.setPassword(password);
        application.createAccount(account);
    }
}
