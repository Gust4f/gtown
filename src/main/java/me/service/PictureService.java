package me.service;

import me.model.nodes.Picture;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Service
public interface PictureService {

    boolean savePicture(final MultipartFile file, final String description, final String userName);

    Optional<Picture> getPicture(final String name);

    long getPictureCount();

    List<Picture> getPictures();
}
