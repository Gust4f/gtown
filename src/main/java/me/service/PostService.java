package me.service;

import com.stormpath.sdk.account.Account;
import me.model.nodes.Post;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public interface PostService {

    int getPostCount();
    boolean exists(final Post post);
    boolean exists(final String postId);

    Optional<Post> getPost(final long page);

    Map<String, Object> getPostDataFromPageNr(final Integer page);

    List<Post> getLatestFivePosts();

    void updatePost(final String postId, final String header, final String body);
    void deletePost(final String postId);
    void insertPost(final String username, final String header, final String body);

    void createCommentOnPost(final String username, final String body, final String postId);
    void deleteCommentOnPost(final Account account, final String commentId, final String postId);
}
