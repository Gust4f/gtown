package me.service;

import org.springframework.stereotype.Service;

@Service
public interface MailSendService {

    boolean sendMessage(final String name, final String from, final String check, final String body);

}
