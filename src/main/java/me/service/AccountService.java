package me.service;

import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface AccountService {

    boolean saveAccount(final String login,
                        final String givenName,
                        final String surName,
                        final String userName,
                        final String email,
                        final String currentPassword,
                        final String newPassword,
                        final String rnewPassowrd);

    String sendResetPasswordEmail(final String email);

    boolean removeAccount(final String login, final String password);

    Map<String, String> createAccount(final String givenName,
                                      final String surname,
                                      final String username,
                                      final String email,
                                      final String password);
}
