package me.web.interceptor;

import me.service.AuthenticationCheckService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public final class UserInterceptor implements HandlerInterceptor {

    private final static Logger LOGGER = LoggerFactory.getLogger(UserInterceptor.class);

    private final AuthenticationCheckService checkService;

    @Autowired
    public UserInterceptor(final AuthenticationCheckService checkService) {
        this.checkService = checkService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        checkService.getAccount(request).ifPresent(account -> {
            final StringBuilder logBuilder = new StringBuilder();
            logBuilder.append("Request from IP: ").append(request.getRemoteAddr());
            logBuilder.append(", authenticated as: " + account.getUsername());
            LOGGER.info(logBuilder.toString());
        });
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView mav)
            throws Exception {
        if (mav != null) {
            mav.addObject("user", checkService.getUsername(request));
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception e)
            throws Exception {

    }
}