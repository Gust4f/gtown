package me.web.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class HeaderInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        response.addHeader(("X-Frame-Options"), "SAMEORIGIN");
        response.addHeader(("Strict-Transport-Security"), "max-age=31536000 ; includeSubDomains");
        response.addHeader(("X-XSS-Protection"), "1; mode=block");
        response.addHeader(("Cache-Control"), "no-cache, no-store, max-age=0, must-revalidate, private;");
        response.addHeader(("Pragma"), "no-cache");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
