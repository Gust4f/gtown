package me.web.interceptor;

import com.maxmind.geoip.LookupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public final class CountryInterceptor implements HandlerInterceptor {

    private final static Logger LOGGER = LoggerFactory.getLogger(CountryInterceptor.class);

    private final LookupService lookupService;

    @Autowired
    public CountryInterceptor(final LookupService lookupService) {
        this.lookupService = lookupService;
    }

    private boolean isCountryAllowed(final String countryCode) {
        return "SE".equals(countryCode) || "DK".equals(countryCode) ||
                "NO".equals(countryCode) || "NL".equals(countryCode);
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getRemoteAddr().equals("172.17.0.1") || request.getRemoteAddr().contains("192.168.0")) {
            return true;
        }
        final String countryCode = lookupService.getCountry(request.getRemoteAddr()).getCode();
        if (countryCode != null && isCountryAllowed(countryCode)) {
            return true;
        }
        final StringBuilder logBuilder = new StringBuilder();
        logBuilder.append("[BLOCKED] Request from IP: ").append(request.getRemoteAddr())
                .append(", linked to country: " + countryCode);
        LOGGER.info(logBuilder.toString());
        response.sendRedirect("http://hasthelargehadroncolliderdestroyedtheworldyet.com");
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
