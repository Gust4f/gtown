package me.web.controller;

import me.exception.UnavailableResourceException;
import me.service.AuthenticationCheckService;
import me.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
final class BlogController {

    private final static String REDIRECT_TO_BLOG = "redirect:/blog";

    private final PostService postService;
    private final AuthenticationCheckService auth;

    @Autowired
    BlogController(final PostService postService,
                   final AuthenticationCheckService auth) {
        this.postService = postService;
        this.auth = auth;
    }

    @RequestMapping(value = "/blog", method = RequestMethod.GET)
    String getBlog(final @RequestParam(required = false) String status,
                   final Model model) {
        if (status != null) {
            model.addAttribute("status", status);
        }
        addPostToModel(postService.getPostCount(), model);
        return getPage(model);
    }

    @RequestMapping(value = "/blog/post/{post}", method = RequestMethod.GET)
    String getPost(final @PathVariable("post") Integer post,
                   final Model model) {
        addPostToModel(post, model);
        return getPage(model);
    }


    private void addPostToModel(final Integer post, final Model model) {
        final int postCount = postService.getPostCount();
        model.addAttribute("numberOfPosts", postCount);
        if (isInvalidPost(post)) {
            throw new UnavailableResourceException("Invalid post index requested: " + post);
        } else {
            model.addAttribute("currentPost", post);
            model.addAllAttributes(postService.getPostDataFromPageNr(post));
        }
    }

    private boolean isInvalidPost(final Integer post) {
        final int postCount = postService.getPostCount();
        return post == null || post < 1 || post > postCount;
    }

    private String getPage(final Model model) {
        model.addAttribute("title", "GTown Blog");
        model.addAttribute("blog", "active");
        return "blog";
    }

    @RequestMapping(value = "/blog/getFragment", method = RequestMethod.GET)
    String getBlogFragment(final Model model) {
        addPostToModel(postService.getPostCount(), model);
        return "fragments/blogfragment";
    }

    @RequestMapping(value = "/blog/getPostFragment/{post}", method = RequestMethod.GET)
    String getPostFragment(final @PathVariable("post") Integer post,
                           final Model model) {
        addPostToModel(post, model);
        return "fragments/blogfragment";
    }

    @RequestMapping(value = "/blog/addPost", method = RequestMethod.GET)
    String getAddPostEditorPage(final Model model) {
        model.addAttribute("title", "Post Editor");
        model.addAttribute("blog", "active");
        return "addpost";
    }

    @RequestMapping(value = "/blog/editPost/{post}", method = RequestMethod.GET)
    String getEditPostEditorPage(final @PathVariable("post") Integer post,
                                 final Model model) {
        model.addAttribute("title", "Post Editor");
        model.addAttribute("blog", "active");
        return getEditPostView(model, post, "addpost");
    }

    private String getEditPostView(final Model model, final int post, final String view) {
        return postService.getPost(post).map(data -> {
            model.addAttribute("post", data);
            return view;
        }).orElse(REDIRECT_TO_BLOG);
    }

    @RequestMapping(value = "/blog/addPostFragment", method = RequestMethod.GET)
    String getAddPostEditorFragment(final Model model) {
        model.addAttribute("title", "Post Editor");
        model.addAttribute("blog", "active");
        return "fragments/addpostfragment";
    }

    @RequestMapping(value = "/blog/editPostFragment/{post}", method = RequestMethod.GET)
    String getEditPostEditorFragment(final @PathVariable("post") int post,
                                     final Model model) {
        model.addAttribute("title", "Post Editor");
        model.addAttribute("blog", "active");
        return getEditPostView(model, post, "fragments/addpostfragment");
    }

    @RequestMapping(value = "/blog/addPost", method = RequestMethod.POST)
    String addPost(final @RequestParam(required = false) String postId,
                   final @RequestParam(required = true) String header,
                   final @RequestParam(required = true) String body,
                   final HttpServletRequest request) {
        return handleBlogPost(postId, header, body, auth.getUsername(request));
    }

    private String handleBlogPost(final String postId, final String header,
                                  final String body, final String userName) {
        if (postId != null && postService.exists(postId)) {
            postService.updatePost(postId, header, body);
        } else {
            postService.insertPost(userName, header, body);
        }
        return REDIRECT_TO_BLOG;
    }
    @RequestMapping(value = "/blog/deletePost/{postId}", method = RequestMethod.GET)
    String deletePost(final @PathVariable("postId") String postId,
                      final Model model) {
        postService.deletePost(postId);
        model.addAttribute("info", "Post was deleted.");
        return getBlog(null, model);
    }

    @RequestMapping(value = "/blog/addComment", method = RequestMethod.POST)
    String addComment(final @RequestParam(required = true) String post,
                      final @RequestParam(required = true) String postId,
                      final @RequestParam(required = true) String body,
                      final HttpServletRequest request) {
        if (!isValidPostParam(post) || postId == null) {
            return REDIRECT_TO_BLOG;
        }
        postService.createCommentOnPost(auth.getUsername(request), body, postId);
        return REDIRECT_TO_BLOG + "/post/" + post;
    }

    private boolean isValidPostParam(final String post) {
        if (post == null) {
            return false;
        }
        final int pageNr = Integer.parseInt(post);
        final int numberOfPosts = postService.getPostCount();
        return !(pageNr > numberOfPosts || pageNr < 1);
    }

    @RequestMapping(value = "/blog/removeComment", method = RequestMethod.POST)
    String removeComment(final @RequestParam(required = true) String commentid,
                         final @RequestParam(required = true) String post,
                         final @RequestParam(required = true) String postId,
                         final HttpServletRequest request) {
        if (!isValidPostParam(post)) {
            return REDIRECT_TO_BLOG;
        }
        if (commentid != null) {
            auth.getAccount(request).ifPresent(account -> postService.deleteCommentOnPost(account, commentid, postId));
        }
        return REDIRECT_TO_BLOG + "/post/" + post;
    }
}
