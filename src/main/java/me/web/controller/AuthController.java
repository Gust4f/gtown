package me.web.controller;

import com.stormpath.sdk.resource.ResourceException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
final class AuthController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    String getPage(final HttpServletRequest request, final Model model) {
        Exception exception = (Exception) request.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        if (exception != null) {
            model.addAttribute("error", ((ResourceException) exception.getCause()).getStormpathError().getMessage());
        }
        return "login";
    }
}
