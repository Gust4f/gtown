package me.web.controller;

import me.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
final class RegistrationController {

    private final AccountService accountService;

    @Autowired
    RegistrationController(final AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    String getRegistrationPage(final Model model) {
        model.addAttribute("title", "Registration");
        return "registration";
    }

    @RequestMapping(value = "/register/getFragment", method = RequestMethod.GET)
    String getRegistrationFragment() {
        return "fragments/registrationfragment";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    String register(final @RequestParam(required = true) String givenName,
                    final @RequestParam(required = true) String surname,
                    final @RequestParam(required = true) String username,
                    final @RequestParam(required = true) String email,
                    final @RequestParam(required = true) String password,
                    final Model model) {
        final Map<String, String> status = accountService.createAccount(
                givenName,
                surname,
                username,
                email,
                password);
        model.addAllAttributes(status);
        return "registration";
    }

    @RequestMapping(value = "/registerFragment", method = RequestMethod.POST)
    String registerFragment(final @RequestParam(required = true) String givenName,
                    final @RequestParam(required = true) String surname,
                    final @RequestParam(required = true) String username,
                    final @RequestParam(required = true) String email,
                    final @RequestParam(required = true) String password,
                    final Model model) {
        final Map<String, String> status = accountService.createAccount(
                givenName,
                surname,
                username,
                email,
                password);
        model.addAllAttributes(status);
        return "fragments/registrationfragment";
    }


}
