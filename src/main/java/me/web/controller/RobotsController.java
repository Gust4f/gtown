package me.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
final class RobotsController {

    @RequestMapping(value = "/robots.txt", method = RequestMethod.GET)
    String getRobots(HttpServletRequest request) {
        return "robotsDisallowed"; // TODO
    }

}
