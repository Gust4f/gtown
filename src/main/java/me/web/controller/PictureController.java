package me.web.controller;

import me.exception.UnavailableResourceException;
import me.model.nodes.Picture;
import me.service.AuthenticationCheckService;
import me.service.PictureService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Controller
final class PictureController {

    private final static Logger LOGGER = LoggerFactory.getLogger(PictureController.class);

    private final PictureService pictureService;
    private final AuthenticationCheckService auth;

    @Autowired
    PictureController(final PictureService pictureService,
                      final AuthenticationCheckService auth) {
        this.pictureService = pictureService;
        this.auth = auth;
    }

    @RequestMapping(value = "/pictures", method = RequestMethod.GET)
    String getPage(final Model model) {
        model.addAttribute("title", "GTown Pictures");
        model.addAttribute("pictures", "active");
        model.addAllAttributes(getPictureData(1));
        return "pictures";
    }

    @RequestMapping(value = "/pictures/picture/{picture}", method = RequestMethod.GET)
    String getPicturesPage(final @PathVariable("picture") Integer picture,
                           final Model model) {
        model.addAttribute("title", "GTown Pictures");
        model.addAttribute("pictures", "active");
        if (isInvalidPictureIndex(picture)) {
            throw new UnavailableResourceException("Invalid picture index requested: " + picture);
        }
        model.addAllAttributes(getPictureData(picture));
        return "pictures";
    }

    @RequestMapping(value = "/pictures/getFragment", method = RequestMethod.GET)
    String getPicturesFragment(final @RequestParam(required = false) Optional<Integer> picture,
                               final Model model) {
        if (!picture.isPresent() || isInvalidPictureIndex(picture.get())) {
            model.addAllAttributes(getPictureData(1));
        } else {
            model.addAllAttributes(getPictureData(picture.get()));
        }
        return "fragments/picturesfragment";
    }

    @RequestMapping(value = "/pictures/getPictureFragment/{picture}", method = RequestMethod.GET)
    String getPictureFragment(final @PathVariable("picture") Integer picture,
                              final Model model) {
        if (isInvalidPictureIndex(picture)) {
            throw new UnavailableResourceException("Invalid picture index requested: " + picture);
        }
        model.addAllAttributes(getPictureData(picture));
        return "fragments/picturesfragment";
    }

    private Map<String, Object> getPictureData(final int picture) {
        final int index = picture -1;
        final Map<String, Object> pictureData = new ConcurrentHashMap<>();
        final List<Picture> pictures = pictureService.getPictures();
        pictureData.put("numberOfPictures", pictures.size());
        if (!pictures.isEmpty()) {
            final Picture file = pictures.get(index);
            pictureData.put("currentPicture", picture);
            pictureData.put("header", file.getDate());
            pictureData.put("data", file.getName());
            pictureData.put("uploader", file.getUser().getUsername());
            pictureData.put("description", file.getDescription());
        }
        return Collections.unmodifiableMap(pictureData);
    }

    private boolean isInvalidPictureIndex(final int picture) {
        final int index = picture -1;
        return index > pictureService.getPictureCount() || index < 0;
    }

    @RequestMapping(value = "/pictures/image", method = RequestMethod.GET)
    void getImage(final @RequestParam(required = true) String name,
                  final HttpServletResponse response) {
        pictureService.getPicture(name).ifPresent(picture -> {
            response.setContentType(picture.getContentType());
            response.setHeader("Content-Disposition", "inline; filename=\"" + name + "\"");
            try {
                final InputStream inputStream = new FileInputStream(picture.getUrl());
                IOUtils.copy(inputStream, response.getOutputStream());
            } catch (IOException e) {
                LOGGER.error("Exception while getting image: {}", e.getMessage());
            }
        });
    }

    @RequestMapping(value = "/pictures/upload", method = RequestMethod.POST)
    String uploadPicture(final @RequestParam(required = false) String description,
                         final @RequestParam(required = true) MultipartFile file,
                         final HttpServletRequest request, final Model model) {
        if (!file.isEmpty()) {
            final boolean success = pictureService.savePicture(file, description, auth.getUsername(request));
            if (success) {
                model.addAttribute("uploaded", true);
            }
        }
        return getPage(model);
    }
}
