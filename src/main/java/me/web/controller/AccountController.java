package me.web.controller;

import me.service.AccountService;
import me.service.AuthenticationCheckService;
import me.util.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static me.util.URL.REDIRECT_TO_ACCOUNT;

@Controller
final class AccountController {

    private final static Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;
    private final AuthenticationCheckService authenticationCheckService;

    @Autowired
    AccountController(final AccountService accountService,
                      final AuthenticationCheckService authenticationCheckService) {
        this.accountService = accountService;
        this.authenticationCheckService = authenticationCheckService;
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    String getPage(final @RequestParam(required = false) Optional<String> status,
                   final HttpServletRequest request, final Model model) {
        model.addAttribute("title", "Your Account");
        model.addAttribute("account", "active");
        status.ifPresent(s -> model.addAttribute(s, true));
        authenticationCheckService.getAccount(request).ifPresent(account -> model.addAttribute("account", account));
        return "account";
    }

    @RequestMapping(value = "/account/getFragment", method = RequestMethod.GET)
    String getPageFragment(final Model model, final HttpServletRequest request) {
        authenticationCheckService.getAccount(request).ifPresent(account -> model.addAttribute("account", account));
        return "fragments/accountfragment";
    }

    @RequestMapping(value = "/account/update", method = RequestMethod.POST)
    String updateAccount(final @RequestParam(required = true) String givenName,
                         final @RequestParam(required = true) String surname,
                         final @RequestParam(required = true) String username,
                         final @RequestParam(required = true) String email,
                         final @RequestParam(required = true) String currentPassword,
                         final @RequestParam(required = true) String password,
                         final @RequestParam(required = true) String rpassword,
                         final HttpServletRequest request,
                         final Model model) {
        boolean successful = accountService.saveAccount(
                authenticationCheckService.getUsername(request),
                givenName,
                surname,
                username,
                email,
                currentPassword,
                password,
                rpassword);
        if (successful) {
            model.addAttribute("success", "Your account was updated!");
        } else {
            model.addAttribute("problem", "Your account could not be updated.");
        }
        return getPage(Optional.empty(), request, model);
    }

    @RequestMapping(value = "/account/delete", method = RequestMethod.POST)
    String deleteAccount(final @RequestParam(required = true) String password,
                         final HttpServletRequest request) throws ServletException {
        final boolean success = accountService.removeAccount(authenticationCheckService.getUsername(request), password);
        if(success) {
            request.logout();
            return URL.REDIRECT_TO_INDEX + "?status=accountDeleted";
        }
        return REDIRECT_TO_ACCOUNT;
    }
}
