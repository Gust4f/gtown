package me.web.controller;


import me.service.AccountService;
import me.util.EmailAddressValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
final class ForgotController {

    private final AccountService accountService;

    @Autowired
    ForgotController(final AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(value = "/forgot", method = RequestMethod.GET)
    String getForgotPage(final Model model) {
        model.addAttribute("title", "Forgot Password");
        return "forgot";
    }

    @RequestMapping(value = "/forgot/getFragment", method = RequestMethod.GET)
    String getForgotFragment(final Model model) {
        model.addAttribute("title", "Forgot Password");
        return "fragments/forgotfragment";
    }

    @RequestMapping(value = "/forgot", method = RequestMethod.POST)
    String resetPassword(final @RequestParam(required = true) String email,
                         final Model model) {
        model.addAttribute("title", "Forgot Password");
        if (EmailAddressValidator.isValidEmailAddress(email)) {
            model.addAttribute("status", accountService.sendResetPasswordEmail(email));
        } else {
            model.addAttribute("status", "Invalid email.");
        }
        return "forgot";
    }

    @RequestMapping(value = "/verify", method = RequestMethod.GET)
    String getVerifyPage(final Model model) {
        model.addAttribute("title", "Verify");
        return "verify";
    }
}
