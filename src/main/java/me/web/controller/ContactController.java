package me.web.controller;

import me.service.MailSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
final class ContactController {

    private final MailSendService mailSendService;

    @Autowired
    ContactController(final MailSendService mailSendService) {
        this.mailSendService = mailSendService;
    }

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    String getPage(final Model model) {
        model.addAttribute("title", "GTown Contact");
        model.addAttribute("contact", "active");
        return "contact";
    }

    @RequestMapping(value = "/contact/getFragment", method = RequestMethod.GET)
    String getPageFragment() {
        return "fragments/contactfragment";
    }

    @RequestMapping(value = "/contact/send", method = RequestMethod.POST)
    String contactFormSubmit(final @RequestParam(required = true) String name,
                                       final @RequestParam(required = true) String email,
                                       final @RequestParam(required = true) String check,
                                       final @RequestParam(required = true) String body,
                                       final Model model) {
        final boolean success = mailSendService.sendMessage(name, email, check, body);
        if (success) {
            model.addAttribute("success", true);
        } else {
            model.addAttribute("fail", true);
        }
        return getPage(model);
    }
}
