package me.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
final class ProjectController {

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    String getPage(final Model model) {
        model.addAttribute("title", "GTown Projects");
        model.addAttribute("projects", "active");
        return "projects";
    }

    @RequestMapping(value = "/projects/getFragment", method = RequestMethod.GET)
    String getPageFragment() {
        return "fragments/projectsfragment";
    }
}
