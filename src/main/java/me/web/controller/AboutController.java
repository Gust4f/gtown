package me.web.controller;

import me.config.metrics.Neo4jHealthIndicator;
import me.config.metrics.OwncloudHealthIndicator;
import me.config.metrics.RedisHealthIndicator;
import me.config.metrics.StormpathHealthIndicator;
import me.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.MailHealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
final class AboutController {

    private final ReportService reportService;
    private final MailHealthIndicator mailHealthIndicator;
    private final Neo4jHealthIndicator neo4jHealthIndicator;
    private final StormpathHealthIndicator stormpathHealthIndicator;
    private final RedisHealthIndicator redisHealthIndicator;
    private final OwncloudHealthIndicator owncloudHealthIndicator;

    @Autowired
    AboutController(final ReportService reportService,
                    final MailHealthIndicator mailHealthIndicator,
                    final Neo4jHealthIndicator neo4jHealthIndicator,
                    final StormpathHealthIndicator stormpathHealthIndicator,
                    final RedisHealthIndicator redisHealthIndicator,
                    final OwncloudHealthIndicator owncloudHealthIndicator) {
        this.reportService = reportService;
        this.mailHealthIndicator = mailHealthIndicator;
        this.neo4jHealthIndicator = neo4jHealthIndicator;
        this.stormpathHealthIndicator = stormpathHealthIndicator;
        this.redisHealthIndicator = redisHealthIndicator;
        this.owncloudHealthIndicator = owncloudHealthIndicator;
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    String getPage(final Model model) {
        model.addAttribute("title", "GTown About");
        model.addAttribute("about", "active");
        addReportMapToModel(model);
        addServiceChecksToModel(model);
        return "about";
    }

    private void addReportMapToModel(final Model model) {
        model.addAllAttributes(reportService.getReportMap());
    }

    private void addServiceChecksToModel(final Model model) {
        model.addAttribute("neo4j_status", neo4jHealthIndicator.health().getStatus().equals(Status.UP) ? "online" : "offline");
        model.addAttribute("stormpath_status", stormpathHealthIndicator.health().getStatus().equals(Status.UP) ? "online" : "offline");
        model.addAttribute("redis_status", redisHealthIndicator.health().getStatus().equals(Status.UP) ? "online" : "offline");
        model.addAttribute("email_status", mailHealthIndicator.health().getStatus().equals(Status.UP) ? "online" : "offline");
        model.addAttribute("owncloud_status", owncloudHealthIndicator.health().getStatus().equals(Status.UP) ? "online" : "offline");
    }

    @RequestMapping(value = "/about/getFragment", method = RequestMethod.GET)
    String getPageFragment(final Model model) {
        addReportMapToModel(model);
        addServiceChecksToModel(model);
        return "fragments/aboutfragment";
    }

    @RequestMapping(value = "/about/details", method = RequestMethod.GET)
    String getDetails(final Model model) {
        model.addAttribute("title", "GTown Details");
        return "sitedetails";
    }

    @RequestMapping(value = "/about/getDetailsFragment", method = RequestMethod.GET)
    String getDetailsFragment() {
        return "fragments/sitedetailsfragment";
    }

    @RequestMapping(value = "/about/getMetricsFragment", method = RequestMethod.GET)
    String getMetricsFragment(final Model model) {
        addReportMapToModel(model);
        addServiceChecksToModel(model);
        return "fragments/metricsfragment";
    }
}
