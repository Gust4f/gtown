package me.web.controller;

import me.model.nodes.Post;
import me.service.PostService;
import me.web.view.RSSViewer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
final class RSSController {

    private final PostService postService;
    private final RSSViewer rssViewer;

    @Autowired
    RSSController(final PostService postService,
                  final RSSViewer rssViewer) {
        this.postService = postService;
        this.rssViewer = rssViewer;
    }

    @RequestMapping(value="/rss", method = RequestMethod.GET)
    ModelAndView getFeedInRss() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setView(rssViewer);
        modelAndView.addObject("feedContent", getLastFivePosts());
        return modelAndView;
    }

    private List<Post> getLastFivePosts() {
        return postService.getLatestFivePosts();
    }
}
