package me.web.controller;

import me.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
final class IndexController {

    private final PostService postService;

    @Autowired
    IndexController(final PostService postService) {
        this.postService = postService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    String getPage(final Model model) {
        return getIndex(Optional.empty(), model);
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    String getIndex(final @RequestParam(required = false) Optional<String> status,
                    final Model model) {
        status.ifPresent(s -> model.addAttribute(s, true));
        return getData(model);
    }

    private String getData(final Model model) {
        model.addAttribute("title", "GTown");
        addBaseDataToModel(model);
        model.addAttribute("home", "active");
        return "index";
    }

    private void addBaseDataToModel(final Model model) {
        final int postCount = postService.getPostCount();
        model.addAttribute("currentPost", postCount);
        model.addAllAttributes(postService.getPostDataFromPageNr(postCount));
    }

    @RequestMapping(value = "/index/getFragment", method = RequestMethod.GET)
    String getIndexFragment(final Model model) {
        addBaseDataToModel(model);
        return "fragments/indexfragment";
    }
}
