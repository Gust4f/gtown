package me.web.view;

import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Content;
import com.rometools.rome.feed.rss.Item;
import me.model.nodes.Post;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.feed.AbstractRssFeedView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component("rssViewer")
public class RSSViewer extends AbstractRssFeedView {

    @Override
    protected void buildFeedMetadata(final Map<String, Object> model, final Channel feed,
                                     final HttpServletRequest request) {
        feed.setTitle("GTown");
        feed.setDescription("Thoughts and  adventures of a developer from Sweden");
        feed.setLink("https://justanotherrandomguyontheinternet.com");
        super.buildFeedMetadata(model, feed, request);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected List<Item> buildFeedItems(final Map<String, Object> model,
                                        final HttpServletRequest request,
                                        final HttpServletResponse response) throws Exception {
        final List<Post> listContent = (List<Post>) model.get("feedContent");
        final List<Item> items = new ArrayList<>(listContent.size());

        for(Post post : listContent ){
            final Item item = new Item();

            final Content content = new Content();
            content.setValue(post.getSummary());
            item.setContent(content);

            item.setTitle(post.getHeader());
            item.setLink("https://justanotherrandomguyontheinternet.com/blog/post/" + post.getId());
            item.setPubDate(post.getDate());

            items.add(item);
        }
        return items;
    }
}
