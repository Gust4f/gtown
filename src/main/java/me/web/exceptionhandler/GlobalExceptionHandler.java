package me.web.exceptionhandler;

import me.exception.UnavailableResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({Exception.class})
    ModelAndView generalException(Exception e) {
        LOGGER.error("Unhandled exception: {}", e);
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("status", Integer.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        modelAndView.addObject("title", "500 - If you see a lost developer send him here");
        return modelAndView;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    ModelAndView invalidArgument(Exception e) {
        LOGGER.warn("An invalid argument supplied: {}", e.getMessage());
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("status", Integer.valueOf(HttpStatus.NOT_FOUND.value()));
        modelAndView.addObject("title", "404 - Now look where you ended up");
        return modelAndView;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({NumberFormatException.class, UnavailableResourceException.class})
    ModelAndView invalidIndex(Exception e) {
        LOGGER.warn("An invalid index was supplied in the request: {}", e.getMessage());
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("status", Integer.valueOf(HttpStatus.NOT_FOUND.value()));
        modelAndView.addObject("title", "404 - Now look where you ended up");
        return modelAndView;
    }
}