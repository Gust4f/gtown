package me.exception;

public class UnavailableResourceException extends RuntimeException {

    public UnavailableResourceException(final String message) {
        super(message);
    }
}
