package me.util.jade;

import com.domingosuarez.boot.autoconfigure.jade4j.JadeHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

@JadeHelper("gdate")
public final class DateFormatter {

    public String format(final Date date) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM hh:mm");
        return simpleDateFormat.format(date);
    }

    public String yyyymmdd(final Date date) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(date);
    }
}
