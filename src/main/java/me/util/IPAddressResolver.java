package me.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;

public final class IPAddressResolver {

    private final static Logger LOGGER = LoggerFactory.getLogger(IPAddressResolver.class);
    private final static String UNKNOWN = "Unknown";

    private IPAddressResolver() {}

    public static String getLANAddress(){
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            LOGGER.error("Could not retrieve LAN address due to exception: {}", e);
        }
        return UNKNOWN;
    }

    public static String getWANAddress() {
        final java.net.URL whatismyip = createUrl();
        if (whatismyip != null) {
            try {
                return getIP(whatismyip);
            } catch (IOException e) {
                LOGGER.error("Could not retrieve WAN address due to exception: {}", e);
            }
        }
        return UNKNOWN;
    }

    private static String getIP(final java.net.URL url) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(
                url.openStream()));
        String ip = in.readLine();
        if (in != null) {
            in.close();
        }
        return ip;
    }

    private static java.net.URL createUrl() {
        java.net.URL whatismyip = null;
        try {
            whatismyip = new java.net.URL("http://checkip.amazonaws.com");
        } catch (MalformedURLException e) {
            LOGGER.error("Could not create WAN address due to exception: {}", e);
        }
        return whatismyip;
    }
}
