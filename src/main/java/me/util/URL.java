package me.util;

public final class URL {
    public final static String REDIRECT_TO_LOGIN = "redirect:/login";
    public final static String REDIRECT_TO_INDEX = "redirect:/index";
    public final static String REDIRECT_TO_ACCOUNT = "redirect:/account";
}
