package me.util;

import java.util.regex.Pattern;

public final class EmailAddressValidator {

    private EmailAddressValidator(){}

    /**
     * regex from mkyong
     */
    public static boolean isValidEmailAddress(final String email) {
        final String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        final Pattern pattern = Pattern.compile(emailPattern);
        return !email.trim().isEmpty() && pattern.matcher(email).matches();
    }
}
