package me.util;

public final class BootstrapProgressbarStatus {

    private BootstrapProgressbarStatus(){}

    public final static String OK = "progress-bar-success";
    public final static String WARNING = "progress-bar-warning";
    public final static String DANGER = "progress-bar-danger";
}
