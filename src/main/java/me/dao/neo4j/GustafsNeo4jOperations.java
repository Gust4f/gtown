package me.dao.neo4j;

import org.neo4j.ogm.cypher.Filters;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Composition wrapper to return Java 8 optional instead of null
 *
 * @author Gustaf
 * @See Neo4jOperations
 */
public interface GustafsNeo4jOperations {

    <T> Optional<T> load(Class<T> type, Long id);

    <T> Optional<T> load(Class<T> type, Long id, int depth);

    <T> Collection<T> loadAll(Class<T> type);

    <T> Collection<T> loadAll(Class<T> type, int depth);

    <T> List<T> loadAllSortedByDate(Class<T> entityType, String cypherQuery, Map<String, ?> parameters);

    <T> Collection<T> loadAll(Collection<T> objects, int depth);

    <T> Optional<T> loadByProperty(Class<T> type, String propertyName, Object propertyValue);

    <T> Collection<T> loadAllByProperty(Class<T> type, String propertyName, Object propertyValue);

    <T> Optional<T> loadByProperties(Class<T> type, Filters parameters);

    <T> Collection<T> loadAllByProperties(Class<T> type, Filters parameters);

    <T> Optional<T> save(T entity);

    void delete(Object entity);

    <T> Optional<T> queryForObject(Class<T> entityType, String cypherQuery, Map<String, ?> parameters);

    <T> Iterable<T> queryForObjects(Class<T> entityType, String cypherQuery, Map<String, ?> parameters);


    long count(Class<?> entityClass);
}
