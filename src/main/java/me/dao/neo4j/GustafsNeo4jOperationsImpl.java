package me.dao.neo4j;

import me.model.DatedEntity;
import org.neo4j.ogm.cypher.Filters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.data.neo4j.template.Neo4jOperations;

import java.util.*;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

public class GustafsNeo4jOperationsImpl implements GustafsNeo4jOperations {

    private final static Logger LOGGER = LoggerFactory.getLogger(GustafsNeo4jOperationsImpl.class);

    @Autowired
    private final Neo4jOperations neo4jTemplate;

    public GustafsNeo4jOperationsImpl(final Neo4jOperations neo4jTemplate) {
        this.neo4jTemplate = neo4jTemplate;
    }

    @Override
    public <T> Optional<T> load(Class<T> type, Long id) {
        T result = neo4jTemplate.load(type, id);
        if (result == null) {
            return Optional.empty();
        }
        return Optional.of(result);
    }

    @Override
    public <T> Optional<T> load(Class<T> type, Long id, int depth) {
        T result = neo4jTemplate.load(type, id, depth);
        if (result == null) {
            return Optional.empty();
        }
        return Optional.of(result);
    }

    @Override
    public <T> Collection<T> loadAll(Class<T> type) {
        return neo4jTemplate.loadAll(type);
    }

    @Override
    public <T> Collection<T> loadAll(Class<T> type, int depth) {
        return neo4jTemplate.loadAll(type, depth);
    }

    @Override
    @Cacheable
    public <T> List<T> loadAllSortedByDate(Class<T> type, String cypherQuery, Map<String, ?> parameters) {
        if (!DatedEntity.class.isAssignableFrom(type)) {
            throw new UnsupportedClassVersionError("Class type must inherit from DatedEntity");
        }
        return StreamSupport.stream(queryForObjects(type, cypherQuery, parameters).spliterator(), false)
                .sorted((t, t1) -> ((DatedEntity) t).getDate().before(((DatedEntity) t1).getDate()) ? -1 : 1)
                .collect(toList());
    }

    @Override
    public <T> Collection<T> loadAll(Collection<T> objects, int depth) {
        return neo4jTemplate.loadAll(objects, depth);
    }

    @Override
    public <T> Optional<T> loadByProperty(Class<T> type, String propertyName, Object propertyValue) {
        try {
            T result = neo4jTemplate.loadByProperty(type, propertyName, propertyValue);
            return Optional.of(result);
        } catch (DataRetrievalFailureException e) {
            LOGGER.error("Error loading property: {}", e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public <T> Collection<T> loadAllByProperty(Class<T> type, String propertyName, Object propertyValue) {
        return neo4jTemplate.loadAllByProperty(type, propertyName, propertyValue);
    }

    @Override
    public <T> Optional<T> loadByProperties(Class<T> type, Filters parameters) {
        try {
            T result = neo4jTemplate.loadByProperties(type, parameters);
            return Optional.of(result);
        } catch (DataRetrievalFailureException e) {
            LOGGER.error("Error loading properties: {}", e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public <T> Collection<T> loadAllByProperties(Class<T> type, Filters parameters) {
        return neo4jTemplate.loadAllByProperties(type, parameters);
    }

    @Override
    public <T> Optional<T> save(T entity) {
        T result = neo4jTemplate.save(entity);
        if (result == null) {
            return Optional.empty();
        }
        return Optional.of(result);
    }

    @Override
    public void delete(Object entity) {
        neo4jTemplate.delete(entity);
    }


    @Override
    public <T> Optional<T> queryForObject(Class<T> entityType, String cypherQuery, Map<String, ?> parameters) {
        T result = neo4jTemplate.queryForObject(entityType, cypherQuery, parameters);
        if (result == null) {
            return Optional.empty();
        }
        return Optional.of(result);
    }

    @Override
    public <T> Iterable<T> queryForObjects(Class<T> entityType, String cypherQuery, Map<String, ?> parameters) {
        return neo4jTemplate.queryForObjects(entityType, cypherQuery, parameters);
    }

    @Override
    public long count(Class<?> entityClass) {
        try {
            return neo4jTemplate.count(entityClass);
        } catch (DataRetrievalFailureException e) {
            LOGGER.error("Error loading properties: {}", e.getMessage());
            return 0;
        }
    }
}
