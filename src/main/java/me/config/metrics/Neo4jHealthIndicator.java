package me.config.metrics;

import me.dao.neo4j.GustafsNeo4jOperations;
import me.model.nodes.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public final class Neo4jHealthIndicator implements HealthIndicator {

    private final static Logger LOGGER = LoggerFactory.getLogger(Neo4jHealthIndicator.class);

    private final GustafsNeo4jOperations neo4jOperations;

    @Autowired
    public Neo4jHealthIndicator(final GustafsNeo4jOperations neo4jOperations) {
        this.neo4jOperations = neo4jOperations;
    }

    @Override
    public Health health() {
        try {
            if (neo4jOperations.count(Post.class) > 0) {
                return Health.up().build();
            }
        } catch (Exception e) {
            LOGGER.error("Neo4j Health check failed: {}", e.getMessage());
        }
        return Health.down().build();
    }
}
