package me.config.metrics;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public final class OwncloudHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        return Health.outOfService().build();
    }
}
