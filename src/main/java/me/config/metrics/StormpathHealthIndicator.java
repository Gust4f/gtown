package me.config.metrics;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.client.Client;
import com.stormpath.spring.security.provider.StormpathAuthenticationProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.Iterator;

@Component
public final class StormpathHealthIndicator implements HealthIndicator {

    private final static Logger LOGGER = LoggerFactory.getLogger(StormpathHealthIndicator.class);

    private final Client client;

    @Autowired
    public StormpathHealthIndicator(final Client client) {
        this.client = client;
    }

    @Override
    public Health health() {
        try {
            final Iterator<Account> iterator = client.getAccounts().iterator();
            if (iterator.hasNext()) {
                return Health.up().build();
            }
        } catch (Exception e) {
            LOGGER.error("Stormpath health check failed: {}", e.getMessage());
        }
        return Health.down().build();
    }
}
