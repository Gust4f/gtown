package me.config;

import com.stormpath.spring.config.StormpathWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;

@Configuration
class WebSecurityConfig extends StormpathWebSecurityConfigurerAdapter {

    @Autowired
    private Roles roles;

    @Override
    public void doConfigure(final WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring()
                .antMatchers("/assets/**");
    }

    @Override
    protected void doConfigure(final HttpSecurity http) throws Exception {
        http.headers().defaultsDisabled().cacheControl();
        http.headers().httpStrictTransportSecurity();
        http.headers().frameOptions().sameOrigin();
        http.headers().xssProtection().block(true);
        http.headers().defaultsDisabled().contentTypeOptions();
        http
                .formLogin()
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .loginPage("/login")
                    .failureUrl("/login")
                    .permitAll()
                .and()
                    .authorizeRequests()
                    .antMatchers("/blog/post/9?").permitAll()
                    .antMatchers("blog/getPostFragment/9?").permitAll()
                    .antMatchers("/account/**").fullyAuthenticated()
                    .antMatchers("/blog/getPostFragment/8*").authenticated()
                    .antMatchers("/blog/post/8*").authenticated()
                    .antMatchers("/blog/getPostFragment/7*").authenticated()
                    .antMatchers("/blog/post/7*").authenticated()
                    .antMatchers("/blog/getPostFragment/6*").authenticated()
                    .antMatchers("/blog/post/6*").authenticated()
                    .antMatchers("/blog/getPostFragment/5*").authenticated()
                    .antMatchers("/blog/post/5*").authenticated()
                    .antMatchers("/blog/getPostFragment/4*").authenticated()
                    .antMatchers("/blog/post/4*").authenticated()
                    .antMatchers("/blog/getPostFragment/3*").authenticated()
                    .antMatchers("/blog/post/3*").authenticated()
                    .antMatchers("/blog/getPostFragment/2*").authenticated()
                    .antMatchers("/blog/post/2*").authenticated()
                    .antMatchers("/blog/getPostFragment/1*").authenticated()
                    .antMatchers("/blog/post/1*").authenticated()
                    .antMatchers("/blog/addComment").authenticated()
                    .antMatchers("/blog/removeComment").authenticated()
                    .antMatchers("/blog/addPost").hasAuthority(roles.ADMIN)
                    .antMatchers("/blog/addPostFragment").hasAuthority(roles.ADMIN)
                    .antMatchers("/blog/editPost/**").hasAuthority(roles.ADMIN)
                    .antMatchers("/blog/editPostFragment/**").hasAuthority(roles.ADMIN)
                    .antMatchers("/pictures/upload").hasAuthority(roles.ADMIN)
                .and()
                    .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessUrl("/index")
                .and()
                    .httpBasic()
                .and()
                    .csrf();
    }
}