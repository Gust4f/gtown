package me.config;

import com.rometools.utils.Lists;
import io.undertow.UndertowOptions;
import io.undertow.server.handlers.LearningPushHandler;
import io.undertow.servlet.api.*;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.undertow.UndertowDeploymentInfoCustomizer;
import org.springframework.boot.context.embedded.undertow.UndertowEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Collections;

@Configuration
@Profile({"prod", "dev"})
class EmbeddedServletContainerConfiguration {

    @Bean
    EmbeddedServletContainerFactory servletContainer() {
        final UndertowEmbeddedServletContainerFactory factory = new UndertowEmbeddedServletContainerFactory();

        factory.setBuilderCustomizers(Lists.create(builder -> builder
                .addHttpListener(8080, "0.0.0.0")
                .setServerOption(UndertowOptions.ENABLE_SPDY, true)
                .setServerOption(UndertowOptions.ENABLE_HTTP2, true)
                .setServerOption(UndertowOptions.HTTP2_SETTINGS_ENABLE_PUSH, true)
                .setServerOption(UndertowOptions.HTTP2_HUFFMAN_CACHE_SIZE, 1000)
                .setServerOption(UndertowOptions.HTTP2_SETTINGS_MAX_CONCURRENT_STREAMS, 20)
                .setServerOption(UndertowOptions.HTTP2_SETTINGS_INITIAL_WINDOW_SIZE, 1048576)
        ));
        factory.setAccessLogEnabled(true);

        factory.addDeploymentInfoCustomizers(new UndertowDeploymentInfoCustomizer() {
            @Override
            public void customize(final DeploymentInfo deploymentInfo) {
                deploymentInfo.setDefaultEncoding("UTF-8");
                deploymentInfo.setUrlEncoding("UTF-8");
                deploymentInfo.addInnerHandlerChainWrapper(new LearningPushHandler.Builder().build(Collections.EMPTY_MAP));
                deploymentInfo.addSecurityConstraint(new SecurityConstraint()
                        .addWebResourceCollection(new WebResourceCollection()
                                .addUrlPattern("/*"))
                        .setTransportGuaranteeType(TransportGuaranteeType.CONFIDENTIAL)
                        .setEmptyRoleSemantic(SecurityInfo.EmptyRoleSemantic.PERMIT))
                        .setConfidentialPortManager(exchange -> 443);
            }
        });
        return factory;
    }
}
