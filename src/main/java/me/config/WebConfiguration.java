package me.config;

import com.maxmind.geoip.LookupService;
import me.web.interceptor.CountryInterceptor;
import me.web.interceptor.HeaderInterceptor;
import me.web.interceptor.UserInterceptor;
import org.eclipse.jetty.servlets.GzipFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
class WebConfiguration extends WebMvcConfigurerAdapter {

    @Value("${geoip.dbfile}")
    private String dbfile;

    @Autowired
    private UserInterceptor userInterceptor;

    @Autowired
    private CountryInterceptor countryInterceptor;

    @Autowired
    private HeaderInterceptor headerInterceptor;

    @Autowired
    private Environment env;

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        for (String profile : env.getActiveProfiles()) {
            if ("prod".equals(profile)) {
                registry.addInterceptor(countryInterceptor);
            }
        }
        registry.addInterceptor(userInterceptor);
        registry.addInterceptor(csrfTokenAddingInterceptor());
        registry.addInterceptor(headerInterceptor);
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/assets/**")
                .addResourceLocations("classpath:/static/assets/")
                .setCachePeriod(3600*24*7);
    }

    @Bean
    Filter gzipFilter() {
        return new GzipFilter();
    }

    @Bean
    HandlerInterceptor csrfTokenAddingInterceptor() {
        return new HandlerInterceptorAdapter() {
            @Override
            public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView view) {
                final CsrfToken token = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
                if (token != null && view != null) {
                    view.addObject(token.getParameterName(), token);
                }
            }
        };
    }

    @Bean
    LookupService lookupService() throws IOException {
        return new LookupService(dbfile, LookupService.GEOIP_MEMORY_CACHE);
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/logout");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }
}
