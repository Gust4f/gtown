package me.config;

import me.dao.neo4j.GustafsNeo4jOperations;
import me.dao.neo4j.GustafsNeo4jOperationsImpl;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.neo4j.ogm.config.Configuration;

@org.springframework.context.annotation.Configuration
@EnableNeo4jRepositories("me.dao")
@EnableTransactionManagement
class CustomNeo4jConfiguration extends Neo4jConfiguration {

    @Override
    public SessionFactory getSessionFactory() {
        return new SessionFactory(getConfiguration(), "me.model.nodes");
    }

    @Bean
    public Configuration getConfiguration() {
        Configuration config = new Configuration();
        config
                .driverConfiguration()
                .setDriverClassName("org.neo4j.ogm.drivers.http.driver.HttpDriver")
                .setURI("http://" + System.getenv("NEO4J_PORT_7474_TCP_ADDR") +
                        ":" + System.getenv("NEO4J_PORT_7474_TCP_PORT"));
        return config;
    }

    @Override
    @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public Session getSession() throws Exception {
        return super.getSession();
    }

    @Bean
    @Autowired
    public GustafsNeo4jOperations gustafsNeo4jOperations(final Neo4jOperations neo4jOperations) {
        return new GustafsNeo4jOperationsImpl(neo4jOperations);
    }
}
