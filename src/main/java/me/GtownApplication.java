package me;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import static me.util.IPAddressResolver.getLANAddress;
import static me.util.IPAddressResolver.getWANAddress;

@SpringBootApplication
public class GtownApplication {

    private final static Logger LOGGER = LoggerFactory.getLogger(GtownApplication.class);
    private final static int EXT_PORT = 80;

    public static void main(String[] args) {
        final Environment environment = SpringApplication.run(GtownApplication.class, args).getEnvironment();
        logAccessInfo(environment);
    }

    private static void logAccessInfo(final Environment environment ) {
        LOGGER.info("Access URLs:\n----------------------------------------------------------\n\t" +
                        "Neo4j: \t\thttp://127.0.0.1:{}\n\t" +
                        "Redis: \t\thttp://127.0.0.1:{}\n\t" +
                        "Local: \t\thttp://127.0.0.1:{}\n\t" +
                        "LAN: \t\thttp://{}:{}\n\t" +
                        "WAN: \t\thttp://{}:{}\n----------------------------------------------------------",
                System.getenv("NEO4J_PORT_7474_TCP_PORT"),
                System.getenv("REDIS_PORT_6379_TCP_PORT"),
                environment.getProperty("server.port"),
                getLANAddress(),
                EXT_PORT,
                getWANAddress(),
                EXT_PORT);
    }
}
