(function loginModule($) {
    "use strict";

    $.ajaxSetup({ 
        cache: true 
    });
    
    $('.flexslider').flexslider({
        directionNav: false,
        controlNav: false
    });

    $('.player').YTPlayer({
        mute: true,
        autoPlay: true,
        loop: true,
        startAt:0,
        stopAt: 220,
        gaTrack: false,
        showControls: false,
        containment:'body',
        opacity: 1
    });
})($);