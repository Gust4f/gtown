var loadBlogEvent = new Event('loadBlogEvent');

(function footerModule($) {
    "use strict";

    var $modal = $('#footModal');
    var $dynamicfoot = $('div.dynamicfoot');
    var $label = $('#myModalLabel');

    $.ajaxSetup({
        cache: true
    });

    (function() {
        $(".fancybox").fancybox({
            padding: 0,
            nextClick: false,
            helpers: {
                overlay: {
                    locked: false
                }
            },
            prevEffect: 'none',
            nextEffect: 'none',
            arrows: false
        });
    })();

    var _show = function show() {
        $modal.modal({
            show: true
        });
    };

    var _detectIE = function detectIE() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return true;
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return true;
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // IE 12 => return version number
            return true;
        }
        return false;
    };

    $('#termslink').on('click', function showTermsModal(event) {
        event.preventDefault();
        $dynamicfoot.html($('.terms').html());
        $label.html('Terms and Conditions of Use');
        _show();
    });

    $('#privacylink').on('click', function showPrivacyModal(event) {
        event.preventDefault();
        $dynamicfoot.html($('.policy').html());
        $label.html('Privacy Policy');
        _show();
    });

    $('#hadron-link').on('click', function fearAndLoathing(event) {
        event.preventDefault();
        $('#hadron-link').find('h1').addClass('wave-text');
        $('.eyes').addClass('lsd');
        $('body').addClass('rainbow');
        $('#index').attr('href', 'http://www.ooooiiii.com/');
        $('.navbar.navbar-default').addClass('pulse');
        $('#projects').attr('href', 'http://www.dabadabadab.com/');
        $('#pictures').attr('href', 'http://www.lalalaa.com/');
        $('#blog').attr('href', 'http://www.ooooiiii.com/');
        $('#about').attr('href', 'http://www.dabadabadab.com/');
        $('#contact').attr('href', 'http://www.lalalaa.com/');
        $('#notrack-link').attr('href', 'http://heyyeyaaeyaaaeyaeyaa.com/');
        $('footer').find('a').attr('href', 'http://www.iiiiiiii.com/');
        $('.curl').addClass('hidden');
        $('#navbar').find('span').addClass('spin');
        $('#do-not-track-img').addClass('pulse');
        $('#defence-shield').addClass('pulse');
        $('#wrapper').html('<div id="click-jack"><iframe width="100%" height="600px" src="https://www.youtube.com/embed/uOmtVFQ3WF8?autoplay=1&cc_load_policy=1" frameborder="0" allowfullscreen></iframe></div>');
    });

    /*
     Sammy init
     */
    $(function initApp() {
        if (_detectIE()) {
            document.getElementsByTagName('body').item(0).innerHTML = '<div class="ie text-center">' +
                '<img class="img-responsive" src="/assets/img/cry.gif">' +
                '<p><strong>Internet Explorer is not supported.</strong></p>' +
                '<p><strong>Please use Chrome or Firefox</strong></p>' +
                '</div>';
        } else {
            nav.run();
        }
    });
})($);
