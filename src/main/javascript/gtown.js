var nav = $.sammy('#wrapper', function navigationInit() {
    'use strict';

    var $navbar = $('#navbar');
    var $wrapper = $('#wrapper');

    (function setupNavigationRouting(sammy) {
        var navbarItems = {
            index: 'index',
            projects: 'projects',
            pictures: 'pictures',
            blog: 'blog',
            about: 'about',
            contact: 'contact',
            account: 'account'
        };

        var updateActiveLink = function updateActiveLink(page) {
            var $old = $navbar.find('.active');
            $old.toggleClass('active');
            $old.blur();
            $navbar.find('#' + page).toggleClass('active');
        };

        var handleMobileMenu = function handleMobileMenu() {
            var $toggle = $('.navbar-toggle');
            if ($toggle.is(':visible')) {
                $toggle.click();
            }
        };

        (function initAnimation() {
            $wrapper.on('click', '#left', function() {
                $wrapper.find('.post-body').addClass('animated fadeOutRight');
            });
            $wrapper.on('click', '#right', function() {
                $wrapper.find('.post-body').addClass('animated fadeOutLeft');
            });
        })();

        (function loadDatepicker() {
            var options = {
                format: 'yyyy/mm/dd',
                startDate: '2015-09-07',
                todayHighlight: true,
                showOffDays: false,
                showStartOfWeek: false,
                orientation: 'bottom left',
                endDate: $('.diary-calendar').data('last'),
                selectedDate: $('.diary-calendar').data('selected'),
                container: "#diary"
            };
            $wrapper.on('click', '.glyphicon-calendar', function(event) {
                event.preventDefault();
                $('.diary-calendar').datepicker(options).on('changeDate', function login() {
                    window.location.href = '/login';
                }).datepicker('show');
            });
        })();

        (function touchScreenEvents() {
            $wrapper.on('swiperight', '#diary-wrapper', function(event) {
                $('#diary-wrapper').find('#left a').click();
            });
            $wrapper.on('swipeleft', '#diary-wrapper', function(event) {
                $('#diary-wrapper').find('#right a').click();
            });
            $wrapper.on('swiperight', '#picture-wrapper', function(event) {
                $('#picture-wrapper').find('#media-left a').click();
            });
            $wrapper.on('swipeleft', '#picture-wrapper', function(event) {
                $('#picture-wrapper').find('#media-right a').click();
            });
        })();

        var attachListener = function attachListener(key, page) {
            var url = '/' + page;
            var location = window.location.pathname;
            sammy.get('/blog/post/:post', function getPreviousPost(context) {
                var current = $('#diary').data('current');
                context.load('/blog/getPostFragment/' + context.params.post).swap(function fadeIn() {
                    $('html, body').animate({scrollTop: $("#wrapper").offset().top});
                    $wrapper.removeClass();
                    if (current > context.params.post) {
                        $wrapper.find('.post-body').addClass('container-fluid animated fadeInLeft');
                    } else {
                        $wrapper.find('.post-body').addClass('container-fluid animated fadeInRight');
                    }
                });
            });
            sammy.get('/blog/addPost', function getAddPostEditor(context) {
                context.load('/blog/addPostFragment').swap();
            });
            sammy.get('/blog/editPost/:post', function getEditPostEditor(context) {
                context.load('/blog/editPostFragment/' + context.params.post).swap();
            });
            sammy.get('/pictures/picture/:picture', function getPreviousPost(context) {
                context.load('/pictures/getPictureFragment/' + context.params.picture).swap();
            });
            sammy.get('/about/details', function getSiteDetails(context) {
                context.load('/about/getDetailsFragment').swap();
            });
            sammy.get(url, function getFragment(context) {
                updateActiveLink(page);
                handleMobileMenu();
                if (location === url) {
                    location = null;
                    return;
                }
                $wrapper.addClass('animated fadeOut');
                context.load(url + '/getFragment').swap(function fadeIn() {
                    $wrapper.removeClass();
                    $wrapper.addClass('container-fluid animated fadeIn');
                });
            });
        };

        $.each(navbarItems, attachListener);
    })(this);
});