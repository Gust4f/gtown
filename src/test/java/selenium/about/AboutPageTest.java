package selenium.about;

import com.stormpath.sdk.application.Application;
import org.assertj.core.api.StrictAssertions;
import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import selenium.BasePagePhantomTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class AboutPageTest extends BasePagePhantomTest {

    // TODO - can I use this to authenticate a test user?
    @Autowired
    private Application application;

    @Override
    protected String getPageUrl() {
        return "/about";
    }

    @Test
    public void test_if_bwi_container_is_visisble() {
        assertThat(findFirst("#basic-website-info")).isDisplayed();
    }

    @Test
    public void test_that_creator_data_is_unavailable_for_guests() {
        assertThat(find("#data")).hasSize(0);
    }

    @Test
    public void test_that_creator_data_is_available_for_users() {
        // TODO
    }

    @Test
    public void test_if_progress_bars_are_in_the_DOM() {
        assertThat(find(".progress-bar")).hasSize(8);
    }

    @Test
    public void test_value_insertion_into_progress_bars() {
        FluentList<FluentWebElement> progressBars = find(".progress-bar");
        for (FluentWebElement element : progressBars) {
            StrictAssertions.assertThat(element.getAttribute("aria-valuemin")).containsOnlyDigits();
            String valuenow = element.getAttribute("aria-valuenow");
            assertThat(valuenow).containsOnlyDigits();
        }
    }

    @Test
    public void test_style_insertion_into_memory_progress_bars() {
        FluentList<FluentWebElement> progressBars = find("#memory .progress-bar");
        for (FluentWebElement element : progressBars) {
            String valuenow = element.getAttribute("aria-valuenow");
            assertThat(element.getAttribute("style")).containsSequence(valuenow);
        }
    }

    @Test
    public void test_style_insertion_into_threads_progress_bars() {
        FluentList<FluentWebElement> progressBars = find("#memory .progress-bar");
        for (FluentWebElement element : progressBars) {
            String valuenow = element.getText();
            assertThat(element.getAttribute("style")).containsSequence(valuenow);
        }
    }

    @Test
    public void test_other_metric_value_insertions() {
        FluentList<FluentWebElement> webElements = find("#other .col-xs-3");
        for (FluentWebElement element : webElements) {
            String status = element.getText();
            assertThat(status).isNotEmpty();
            assertThat(element).hasClass(status);
        }
    }

    @Test
    public void test_click_threads_eye_icon() {
        click("#threads .hand");
        assertThat(url()).isEqualTo(baseUrl + "/login");
    }

    @Test
    public void test_click_more_metrics_button() {
        click("#more-metrics");
        assertThat(url()).isEqualTo(baseUrl + "/login");
    }
}
