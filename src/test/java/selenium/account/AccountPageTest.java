package selenium.account;

import org.junit.Ignore;
import org.junit.Test;
import selenium.BasePagePhantomTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

/*
    TODO: Requires authentication..
 */
@Ignore
public class AccountPageTest extends BasePagePhantomTest {

    @Override
    protected String getPageUrl() {
        return "/account";
    }

    @Test
    public void test_that_account_form_was_rendered() {
        assertThat(findFirst("form")).isDisplayed();
        assertThat(findFirst("#given-name")).isDisplayed();
        assertThat(findFirst("#sur-name")).isDisplayed();
        assertThat(findFirst("#email")).isDisplayed();
        assertThat(findFirst("#passsord")).isDisplayed();
        assertThat(findFirst("#npassowrd")).isDisplayed();
        assertThat(findFirst("#nrpassword")).isDisplayed();
    }

    @Test
    public void test_that_it_is_possible_to_submit_account_form() {
        submit("form");
        assertThat(alert()).isPresent().hasText("Your account could not be updated.");
    }

    @Test
    public void test_submit_username_change() {
        fill("#username").with("Testa");
        fill("#password").with("password");
        submit("form");
        assertThat(alert()).isPresent().hasText("Your account was updated!");
    }

    @Test
    public void test_submitting_with_occupied_username() {
        fill("#username").with("Test");
        fill("#password").with("password");
        submit("form");
        assertThat(alert()).isPresent().hasText("Your account could not be updated.");
    }

    @Test
    public void test_submitting_with_invalid_password() {
        fill("#username").with("Test");
        fill("#password").with("       ");
        submit("form");
        assertThat(alert()).isPresent().hasText("Your account could not be updated.");
    }

    @Test
    public void test_submitting_with_invalid_new_password() {
        fill("#username").with("Test");
        fill("#password").with("password");
        fill("#npassword").with("password1");
        fill("nrpassowrd").with("passowrd2");
        submit("form");
        assertThat(alert()).isPresent().hasText("Your account could not be updated.");
    }

    @Test
    public void test_clicking_delete_account() {
        click("#delete-account");
        assertThat(url()).isEqualTo(baseUrl + "/index?status=accountDeleted");
    }
}
