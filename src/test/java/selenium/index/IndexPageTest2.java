package selenium.index;

import org.fluentlenium.assertj.FluentLeniumAssertions;
import org.junit.Test;
import selenium.BasePageFirefoxTest;

import static org.assertj.core.api.StrictAssertions.assertThat;

public class IndexPageTest2 extends BasePageFirefoxTest {

    @Override
    protected String getPageUrl() {
        return "/";
    }

    @Test
    public void test_if_title_is_correct() {
        assertThat(title()).isEqualTo("GTown");
    }

    @Test
    public void test_if_navbar_item_is_active() {
        FluentLeniumAssertions.assertThat(find(".active")).hasSize(1);
        FluentLeniumAssertions.assertThat(find("#index")).hasClass("active");
        takeScreenShot("screenshots/[JS_enabled][Desktop]index_active_in_navbar.png");
    }

    @Test
    public void test_if_page_recognize_guest_users() {
        FluentLeniumAssertions.assertThat(findFirst(".jumbotron")).isDisplayed();
        FluentLeniumAssertions.assertThat(find(".jumbotron h2")).hasText("Hello Guest");
    }

    @Test
    public void test_if_diary_was_loaded() {
        FluentLeniumAssertions.assertThat(findFirst("#diary")).isDisplayed();
        FluentLeniumAssertions.assertThat(findFirst("#diary h2")).isDisplayed();
        FluentLeniumAssertions.assertThat(findFirst("#diary h3")).isDisplayed();
        FluentLeniumAssertions.assertThat(findFirst("#diary .post-body")).isDisplayed();
        FluentLeniumAssertions.assertThat(findFirst("#diary .comment")).isDisplayed();
    }

    @Test
    public void test_if_only_previous_arrow_displayed() {
        int current = Integer.parseInt(findFirst("#diary").getAttribute("data-current"));
        FluentLeniumAssertions.assertThat(findFirst("#left")).isDisplayed();
        assertThat(findFirst("#left a").getAttribute("href")).contains("/blog/post/" + (current - 1));
        FluentLeniumAssertions.assertThat(find("#right")).hasSize(0);
    }

    @Test
    public void test_change_post_by_clicking_previous() {
        String header1 = find("#diary h3").getText();
        String body1 = find("#diary .post-body").getText();

        click("#left");
        awaitNavigation();

        String header2 = find("#diary h3").getText();
        String body2 = find("#diary .post-body").getText();
        assertThat(header1).isNotEqualTo(header2);
        assertThat(body1).isNotEqualTo(body2);
        FluentLeniumAssertions.assertThat(findFirst("#left")).isDisplayed();
        FluentLeniumAssertions.assertThat(findFirst("#right")).isDisplayed();

        takeScreenShot("screenshots/[JS_enabled][Desktop]clicked_on_previous_to_change_post.png");
    }

    @Test
    public void test_if_on_same_page_when_clicking_previous(){
        click("#left");
        assertThat(title()).isEqualTo("GTown");
        FluentLeniumAssertions.assertThat(find("#index")).hasClass("active");
    }

    @Test
    public void test_goto_about_using_learn_more_button() {
        click(".jumbotron .btn-primary");
        awaitNavigation();
        assertThat(url()).isEqualTo(baseUrl + "/about");
        FluentLeniumAssertions.assertThat(find("#about")).hasClass("active");
        takeScreenShot("screenshots/[JS_enabled][Desktop]clicked_on_learn_more_to_navigate.png");
    }

    @Test
    public void test_goto_rss() {
        click("#rss-img");
        assertThat(url()).isEqualTo(baseUrl + "/rss");
        takeScreenShot("screenshots/[JS_enabled][Desktop]clicked_on_rss.png");
    }
}
