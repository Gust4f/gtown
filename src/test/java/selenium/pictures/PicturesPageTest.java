package selenium.pictures;

import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.Test;
import selenium.BasePagePhantomTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class PicturesPageTest extends BasePagePhantomTest {

    @Override
    protected String getPageUrl() {
        return "/pictures";
    }

    @Test
    public void test_upload_form_is_not_shown_for_guests() {
        assertThat(find("form")).hasSize(0);
    }

    @Test
    public void test_that_fragment_alert_is_hidden() {
        assertThat(findFirst("#fragment-error")).isNotDisplayed();
    }

    @Test
    public void test_picture_data_inclusion() {
        assertThat(findFirst("#picture-row").getAttribute("data-current")).containsOnlyDigits();
        assertThat(findFirst("#picture-row").getAttribute("data-total")).containsOnlyDigits();
    }

    @Test
    public void test_that_previous_is_not_displayed() {
        assertThat(find("#media-left")).hasSize(0);
    }

    @Test
    public void test_that_next_is_displayed() {
        assertThat(findFirst("#media-right")).isDisplayed();
    }

    @Test
    public void test_that_you_can_navigate_to_next_picture() {
        String description1 = findFirst("#description").getText();
        String uploader1 = findFirst("#uploader").getText();
        click("#media-right");
        assertThat(url()).isEqualTo(baseUrl + "/pictures/picture/2");
        assertThat(findFirst("#media-right")).isDisplayed();
        assertThat(findFirst("#media-left")).isDisplayed();

        String description2 = findFirst("#description").getText();
        String uploader2 = findFirst("#uploader").getText();

        assertThat(description1).isNotEqualTo(description2);
        assertThat(uploader1).isNotEqualTo(uploader2);
    }

    @Test
    public void test_that_you_can_navigate_to_previous_picture() {
        goTo(baseUrl + "/pictures/picture/2");
        String description1 = findFirst("#description").getText();
        String uploader1 = findFirst("#uploader").getText();
        assertThat(findFirst("#media-right")).isDisplayed();
        assertThat(findFirst("#media-left")).isDisplayed();
        click("#media-left");
        assertThat(url()).isEqualTo(baseUrl + "/pictures/picture/1");
        assertThat(findFirst("#media-right")).isDisplayed();
        assertThat(find("#media-left")).hasSize(0);

        String description2 = findFirst("#description").getText();
        String uploader2 = findFirst("#uploader").getText();

        assertThat(description1).isNotEqualTo(description2);
        assertThat(uploader1).isNotEqualTo(uploader2);
    }

    @Test
    public void test_that_next_is_not_displayed_on_last_picture() {
        goTo(baseUrl + "/pictures?picture=3");
        assertThat(find("#media-right")).hasSize(0);
    }

    @Test
    public void test_so_all_gallery_circles_are_displayed() {
        FluentList<FluentWebElement> webElements = find(".ch-item a");
        assertThat(webElements).hasSize(6);
        for (FluentWebElement element : webElements) {
            assertThat(element).isDisplayed();
            assertThat(element).hasClass("fancybox");
        }
    }

    @Test
    public void test_that_you_can_click_on_gallery_circles_as_guest() {
        click(findFirst(".ch-item a"));
        assertThat(url()).isEqualTo(baseUrl + "/login");
    }
}
