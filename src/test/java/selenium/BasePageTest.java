package selenium;

import me.model.nodes.Comment;
import me.model.nodes.Picture;
import me.model.nodes.Post;
import me.model.nodes.User;
import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.neo4j.template.Neo4jOperations;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.*;

@Profile("test")
public abstract class BasePageTest extends Setup {

    private final static String FILE_NAME = "test";

    @Autowired
    private Neo4jOperations neo4jOperations;

    static {
        System.setProperty("phantomjs.binary", "/usr/bin/phantomjs");
        System.setProperty("webdriver.chrome.driver", "/opt/google/chrome/google-chrome");
    }

    protected String baseUrl;

    protected Dimension maxScaledSize = new Dimension(1600, 900);

    protected Dimension mobile = new Dimension(720, 468);

    protected abstract void awaitNavigation();

    protected abstract String getPageUrl();

    @Before
    public void setUp() throws IOException {
        baseUrl = "http://localhost:" + port;
        cleanDataBases();
        prepareNeo4j();
        goTo(baseUrl + getPageUrl());
    }

    private void cleanDataBases() {
        neo4jOperations.query("MATCH (n) DETACH DELETE n", Collections.emptyMap());
    }

    /*
        Okay to use Date here since they don't really matter - suppressing warnings.
     */
    @SuppressWarnings("deprecation")
    private void prepareNeo4j() {
        final User test1 = createUser("Test1");
        final User test2 = createUser("Test2");
        final User test3 = createUser("Test3");
        neo4jOperations.save(createPost("Test1", test1, new Date(2010, 10, 10), "Test1"));
        neo4jOperations.save(createPost("Test2", test2, new Date(2011, 11, 11), "Test2"));
        neo4jOperations.save(createPostWithComment("Test3", test3, new Date(2012, 12, 12), "Test3"));

        neo4jOperations.save(addPicture(test1, "Test A"));
        neo4jOperations.save(addPicture(test2, "Test B"));
        neo4jOperations.save(addPicture(test3, "Test C"));
    }

    private User createUser(final String username) {
        final User user = new User();
        user.setUsername(username);
        return user;
    }

    private Picture addPicture(User user, String description) {
        final Picture picture = new Picture();
        picture.setUser(user);
        picture.setContentType("image/png");
        picture.setName(FILE_NAME);
        picture.setDescription(description);
        picture.setUrl("src/test/input.png");
        return picture;
    }


    private Post createPost(String header, User author, Date date, String body) {
        final Post post = new Post();
        post.setUser(author);
        post.setDate(date);
        post.setHeader(header);
        post.setBody(body);
        return post;
    }

    private Post createPostWithComment(String header, User author, Date date, String body) {
        final Post post = new Post();
        post.setUser(author);
        post.setDate(date);
        post.setHeader(header);
        post.setBody(body);
        post.addComment(createComment(author, date));
        return post;
    }

    private Comment createComment(User author, Date date) {
        final Comment comment = new Comment();
        comment.setUser(author);
        comment.setDate(date);
        comment.setBody("This is a test comment");
        return comment;
    }

    @Test
    public void test_if_meta_tags_are_as_expected() {
        assertThat(find("meta", withName("robots"))).hasSize(1);
        assertThat(find("meta", withName("googlebot"))).hasSize(1);
        assertThat(find("meta", withName("viewport"))).hasSize(1);
        assertThat(find("meta[charset='utf-8']")).hasSize(1);
    }

    @Test
    public void test_if_stylesheets_are_available() {
        FluentList<FluentWebElement> cssLinks = find("link");
        assertThat(cssLinks.get(0).getAttribute("href")).isEqualTo(baseUrl + "/assets/css/bootstrap.min.css");
        assertThat(cssLinks.get(1).getAttribute("href")).isEqualTo(baseUrl + "/assets/css/gtown.min.css");
    }

    @Test
    public void test_if_navbar_isDisplayed() {
        assertThat(findFirst(".navbar")).isDisplayed();
    }

    @Test
    public void test_if_footer_is_displayed() {
        assertThat(findFirst("footer")).isDisplayed();
    }

    @Test
    public void test_if_modal_in_footer_is_hidden_on_load() {
        assertThat(findFirst("#footModal")).isNotDisplayed();
    }

    @Test
    public void test_if_footer_links_are_correct() {
        FluentList<FluentWebElement> links = find(".info");
        assertThat(links.get(0).getAttribute("href")).isEqualTo("http://www.chalmers.se/");
        assertThat(links.get(1).getAttribute("href")).isEqualTo("http://www.gu.se/");
    }

    @Test
    public void test_if_scripts_are_included() {
        FluentList<FluentWebElement> javascripts = find("script");
        assertThat(javascripts.get(0).getAttribute("src")).isEqualTo(baseUrl + "/assets/js/lib/jquery.min.js");
        assertThat(javascripts.get(1).getAttribute("src")).isEqualTo(baseUrl + "/assets/js/lib/bootstrap.min.js");
        assertThat(javascripts.get(2).getAttribute("src")).isEqualTo(baseUrl + "/assets/js/jquery.fancybox.pack.js");
        assertThat(javascripts.get(3).getAttribute("src")).isEqualTo(baseUrl + "/assets/js/lib/sammy-latest.min.js");
        assertThat(javascripts.get(4).getAttribute("src")).isEqualTo(baseUrl + "/assets/js/gtown.min.js");
    }

    @Test
    public void test_if_policy_loaded_and_hidden() {
        assertThat(findFirst(".policy")).isNotDisplayed();
    }

    @Test
    public void test_if_terms_loaded_and_hidden() {
        assertThat(findFirst(".terms")).isNotDisplayed();
    }
}
