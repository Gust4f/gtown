package selenium;

import org.junit.Ignore;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Ignore
public abstract class BasePageFirefoxTest extends BasePageTest {

    protected WebDriver driver = new FirefoxDriver();

    @Override
    public WebDriver getDefaultDriver() {
        driver.manage().window().setSize(mobile);
        return driver;
    }

    @Override
    protected void awaitNavigation() {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(".jumbotron")));
    }
}
