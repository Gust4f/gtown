package selenium.contact;

import org.junit.Test;
import selenium.BasePagePhantomTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class ContactPageTest extends BasePagePhantomTest {

    @Override
    protected String getPageUrl() {
        return "/contact";
    }

    @Test
    public void test_if_contact_form_was_rendered() {
        assertThat(findFirst("#contact-form")).isDisplayed();
        assertThat(findFirst("#name")).isDisplayed();
        assertThat(findFirst("#email")).isDisplayed();
        assertThat(findFirst("#inputReal")).isDisplayed();
        assertThat(findFirst("#body")).isDisplayed();
        assertThat(findFirst("#send")).isDisplayed();
    }

    @Test
    public void test_that_alerts_are_not_displayed() {
        assertThat(find("#fail")).hasSize(0);
        assertThat(find("#success")).hasSize(0);
    }

    @Test
    public void test_fill_contact_form_with_an_invalid_email() {
        fill("#name").with("Test");
        fill("#email").with("Test");
        fill("#body").with("There is no spoon");
        submit("form");

        assertThat(findFirst("#fail")).isDisplayed();
    }

    @Test
    public void test_fill_contact_form_with_no_body() {
        fill("#name").with("Test");
        fill("#email").with("test@test.com");
        fill("#body").with("");
        submit("form");

        assertThat(findFirst("#fail")).isDisplayed();
    }

    @Test
    public void test_fill_contact_form_with_inputReal() {
        fill("#name").with("Test");
        fill("#email").with("test@test.com");
        fill("#inputReal").with("I'm a robot filling forms");
        fill("#body").with("There is no spoon");
        submit("form");

        assertThat(findFirst("#fail")).isDisplayed();
    }

    @Test
    public void test_fill_contact_form_with_valid_input() {
        fill("#name").with("Test");
        fill("#email").with("test@test.com");
        fill("#body").with("There is no spoon");
        submit("form");

        assertThat(findFirst("#success")).isDisplayed();
    }
}
