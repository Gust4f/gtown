package selenium.error;

import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

public class ErrorPageDesktopTest extends ErrorPageTest {

    private WebDriver driver = new PhantomJSDriver();

    @Override
    public WebDriver getDefaultDriver() {
        driver.manage().window().setSize(maxScaledSize);
        return driver;
    }

    @BeforeClass
    public static void setUp() {
        ENV = "Desktop";
    }
}
