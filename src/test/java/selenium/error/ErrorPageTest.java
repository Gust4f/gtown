package selenium.error;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import selenium.Setup;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

@Ignore
public class ErrorPageTest extends Setup {

    static {
        System.setProperty("phantomjs.binary", "/usr/bin/phantomjs");
    }

    public static String ENV;

    protected int serverPort = 9090;

    protected String baseUrl = "http://localhost:" + serverPort;

    protected Dimension maxScaledSize = new Dimension(1600, 900);

    protected Dimension mobile = new Dimension(720, 468);

    @Test
    public void test_if_404_page_was_shown_for_invalid_page_request() {
        goTo(baseUrl + "/awdawdawdawd");
        assertThat(find(".body-404")).hasSize(1);
        assertThat(title()).isEqualTo("404 - Now look where you ended up");
        takeScreenShot("screenshots/[JS_enabled][" + ENV + "]404.png");
    }

    @Test
    public void test_if_500_page_was_shown_for_invalid_request() {
        goTo(baseUrl + "/blog?post=invalid");
        assertThat(find(".body-500")).hasSize(1);
        assertThat(title()).isEqualTo("500 - If you see a lost developer send him here");
        takeScreenShot("screenshots/[JS_enabled][" + ENV + "]500.png");
    }
}
