package selenium;

import me.GtownApplication;
import org.fluentlenium.adapter.FluentTest;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GtownApplication.class)
@WebIntegrationTest("server.port:0")
@TestPropertySource(locations="classpath:test.properties")
@Profile("test")
public abstract class Setup extends FluentTest {

    @Value("${local.server.port}")
    protected int port;
}
