package selenium;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

@Ignore
public class NavigationHashTest extends BasePageTest {

    private WebDriver driver = new PhantomJSDriver();

    @Override
    public WebDriver getDefaultDriver() {
        driver.manage().window().setSize(mobile);
        return driver;
    }

    @Override
    protected void awaitNavigation() {}

    @Override
    protected String getPageUrl() {
        return "/index";
    }

    /**
     * After some user feedback:
     *
     * The dynamic loading of fragments removed the
     * browsing history. This test makes sure that
     * history works when using the JS navigation.
     */
    @Test
    public void test_js_navigation_and_check_history() {
        assertThat(find("#diary")).hasSize(1);
        click("#about");
        assertThat(find("#diary")).hasSize(0);
        back();
        assertThat(url()).isEqualTo(baseUrl + getPageUrl());
        assertThat(find("#diary")).hasSize(1);
        forward();
        assertThat(find("#diary")).hasSize(0);
        assertThat(url()).isEqualTo(baseUrl + "/about");
    }

    private void back() {
        driver.navigate().back();
    }

    private void forward() {
        driver.navigate().forward();
    }
}
