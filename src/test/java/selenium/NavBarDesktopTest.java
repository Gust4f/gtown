package selenium;

import org.assertj.core.api.StrictAssertions;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;

/**
 * Tests the navbar as a Guest user with javascript enabled.
 */
public class NavBarDesktopTest extends BasePageTest {

    private WebDriver driver = new PhantomJSDriver();

    @Override
    public WebDriver getDefaultDriver() {
        driver.manage().window().setSize(maxScaledSize);
        return driver;
    }

    @Override
    protected String getPageUrl() {
        return "/";
    }

    @Override
    protected void awaitNavigation() {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(".jumbotron")));
    }

    @Before
    public void setUp() {
        goTo(baseUrl);
        assertThat(title()).isEqualTo("GTown");
    }

    @Test
    public void test_goto_projects_using_navbar() {
        click("#projects");
        awaitNavigation();
        assertThat(find(".active")).hasSize(1);
        assertThat(title()).isEqualTo("GTown");
        assertThat(url()).isEqualTo(baseUrl + "/projects");
        assertThat(find("#projects")).hasClass("active");
        takeScreenShot("screenshots/[JS_enabled][Desktop]clicked_on_projects_to_navigate.png");
    }

    @Test
    public void test_goto_pictures_using_navbar() {
        click("#pictures");
        awaitNavigation();
        assertThat(find(".active")).hasSize(1);
        assertThat(title()).isEqualTo("GTown");
        assertThat(url()).isEqualTo(baseUrl + "/pictures");
        assertThat(find("#pictures")).hasClass("active");
        takeScreenShot("screenshots/[JS_enabled][Desktop]clicked_on_pictures_to_navigate.png");
    }

    @Test
    public void test_goto_blog_using_navbar() {
        click("#blog");
        awaitNavigation();
        assertThat(find(".active")).hasSize(1);
        assertThat(title()).isEqualTo("GTown");
        assertThat(url()).isEqualTo(baseUrl + "/blog");
        assertThat(find("#blog")).hasClass("active");
        takeScreenShot("screenshots/[JS_enabled][Desktop]clicked_on_blog_to_navigate.png");
    }

    @Test
    public void test_goto_about_using_navbar() {
        click("#about");
        awaitNavigation();
        assertThat(find(".active")).hasSize(1);
        assertThat(title()).isEqualTo("GTown");
        assertThat(url()).isEqualTo(baseUrl + "/about");
        assertThat(find("#about")).hasClass("active");
        takeScreenShot("screenshots/[JS_enabled][Desktop]clicked_on_about_to_navigate.png");
    }

    @Test
    public void test_goto_contact_using_navbar() {
        click("#contact");
        awaitNavigation();
        assertThat(find(".active")).hasSize(1);
        assertThat(title()).isEqualTo("GTown");
        assertThat(url()).isEqualTo(baseUrl + "/contact");
        assertThat(find("#contact")).hasClass("active");
        takeScreenShot("screenshots/[JS_enabled][Desktop]clicked_on_contact_to_navigate.png");
    }

    @Test
    public void test_goto_login_using_navbar() {
        click("#login");
        awaitNavigation();
        assertThat(url()).isEqualTo(baseUrl + "/login");
        takeScreenShot("screenshots/[JS_enabled][Desktop]clicked_on_login_to_navigate.png");
    }

    /*
        PhantomJS cannot handle the page curl
     */
    @Test
    public void test_goto_bitbucket_using_curl() {
        click(".curl");
        awaitNavigation();
        StrictAssertions.assertThat(url()).isEqualTo("https://bitbucket.org/account/signin/?next=/Gust4f/gtown");
        takeScreenShot("screenshots/[JS_enabled]clicked_on_bitbucket_source.png");
    }

    @Test
    public void test_that_account_is_not_available() {
        assertThat(find("#account")).hasSize(0);
    }

    @Test
    public void test_that_logout_is_not_available() {
        assertThat(find("#logout")).hasSize(0);
    }
}
