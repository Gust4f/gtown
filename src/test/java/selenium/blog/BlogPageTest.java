package selenium.blog;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.BasePagePhantomTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class BlogPageTest extends BasePagePhantomTest {

    @Override
    protected String getPageUrl() {
        return "/blog";
    }

    @Test
    public void test_if_title_is_correct() {
        assertThat(title()).isEqualTo("GTown Blog");
    }

    @Test
    public void test_if_navbar_item_is_active() {
        assertThat(find(".active")).hasSize(1);
        assertThat(find("#blog")).hasClass("active");
    }

    @Test
    public void test_that_fragment_error_is_hidden() {
        assertThat(findFirst("#fragment-error")).isNotDisplayed();
    }

    @Test
    public void test_that_comment_box_is_not_available() {
        assertThat(find("#comment-form")).hasSize(0);
    }

    @Test
    public void test_that_diary_is_displayed() {
        assertThat(findFirst("#diary-wrapper")).isDisplayed();
        assertThat(findFirst("#diary")).isDisplayed();
        assertThat(findFirst("#diary").getAttribute("data-current")).isEqualTo("3");
        assertThat(findFirst("#diary").getAttribute("data-total")).isEqualTo("3");
        assertThat(findFirst("#diary h2")).isDisplayed();
        assertThat(findFirst("#diary h3")).isDisplayed();
        assertThat(findFirst("#diary .post-body")).isDisplayed();
        assertThat(findFirst("#diary .comment")).isDisplayed();
    }

    @Test
    public void test_if_only_previous_arrow_displayed() {
        assertThat(findFirst("#left")).isDisplayed();
        assertThat(findFirst("#left a").getAttribute("href")).contains("/blog/post/" + (getCurrentPost() - 1));
        assertThat(find("#right")).hasSize(0);
    }

    @Test
    public void test_if_only_next_arrow_displayed() {
        goTo(baseUrl + getPageUrl() + "/post/1");
        awaitNavigation();
        assertThat(findFirst("#right a").getAttribute("href")).contains("/blog/post/2");
        assertThat(find("#left")).hasSize(0);
    }

    public void test_if_next_arrow_works() {
        goTo(baseUrl + getPageUrl() + "/post/1");
        assertThat(findFirst("#right a").getAttribute("href")).contains("/blog/post/2");
    }

    private int getCurrentPost() {
        return Integer.parseInt(findFirst("#diary").getAttribute("data-current"));
    }

    /*
        TODO: Refactor - too big, must be able to solve in a better way.
     */
    @Test
    public void test_post_navigation() {
        String header1 = find("#diary h3").getText();
        String body1 = find("#diary .post-body").getText();

        int current1 = getCurrentPost();

        click("#left");
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.textToBePresentInElement(findFirst("#diary h3").getElement(), "Test2"));

        int current2 = getCurrentPost();

        String header2 = find("#diary h3").getText();
        String body2 = find("#diary .post-body").getText();
        assertThat(header1).isNotEqualTo(header2);
        assertThat(body1).isNotEqualTo(body2);
        assertThat(findFirst("#left")).isDisplayed();
        assertThat(findFirst("#right")).isDisplayed();
        assertThat(current2).isEqualTo(current1 - 1);
        assertThat(find("#diary .comment")).hasSize(0);
        assertThat(url()).isEqualTo(baseUrl + getPageUrl() + "/post/" + (current1 - 1));

        click("#right");
        webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.textToBePresentInElement(findFirst("#diary h3").getElement(), "Test3"));

        int current3 = getCurrentPost();

        String header3 = find("#diary h3").getText();
        String body3 = find("#diary .post-body").getText();
        assertThat(header1).isNotEqualTo(header2);
        assertThat(body1).isNotEqualTo(body2);
        assertThat(header1).isEqualTo(header3);
        assertThat(body1).isEqualTo(body3);
        assertThat(current1).isEqualTo(current3);
        assertThat(findFirst("#left")).isDisplayed();
        assertThat(find("#right")).hasSize(0);
        assertThat(url()).isEqualTo(baseUrl + getPageUrl() + "/post/" + current3);
        assertThat(findFirst("#diary .comment")).isDisplayed();
    }
}
