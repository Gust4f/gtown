package selenium.authentication;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class ForgotPasswordPageTest extends StormpathPageTest {

    @BeforeClass
    public static void init() {
        ENV = "Desktop";
    }

    @Before
    public void setUp() {
        goTo(FORGOT);
    }

    @Test
    public void test_invalid_form_submission() {
        fill(".form-control").with("Test");
        submit(".login-form");
        assertThat(alert()).isPresent().hasText("Invalid email address.");
    }

    @Test
    public void test_invalid_form_submission2() {
        fill(".form-control").with("Test@test");
        submit(".login-form");
        assertThat(alert()).isPresent().hasText("Invalid email address.");
    }

    @Test
    public void test_invalid_form_submission3() {
        fill(".form-control").with("Test@Test@Test.com");
        submit(".login-form");
        assertThat(alert()).isPresent().hasText("Invalid email address.");
    }

    @Test
    public void test_valid_form_submission_with_unregistered_email() {
        fill(".form-control").with("Test@Test.com");
        submit(".login-form");
        assertThat(alert()).isPresent().hasText("Invalid email address.");
    }

    @Test
    public void test_go_back_to_login_page() {
        click(".to-login");
        assertThat(url()).isEqualTo(LOGIN);
    }
}
