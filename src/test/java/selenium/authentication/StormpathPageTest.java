package selenium.authentication;

import org.junit.Ignore;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import selenium.Setup;

@Ignore
public class StormpathPageTest extends Setup {

    static {
        System.setProperty("phantomjs.binary", "/usr/bin/phantomjs");
    }

    protected static String ENV;

    protected final static int PORT = 9090;

    protected final static String BASE_URL = "http://localhost:" + PORT;

    protected final static String LOGIN = BASE_URL + "/login";
    protected final static String FORGOT = BASE_URL + "/forgot";
    protected final static String REGISTER = BASE_URL + "/register";

    protected final Dimension maxScaledSize = new Dimension(1600, 900);

    protected final Dimension mobile = new Dimension(720, 468);

    protected final WebDriver driver = new PhantomJSDriver();

    @Override
    public WebDriver getDefaultDriver() {
        driver.manage().window().setSize(mobile);
        return driver;
    }
}
