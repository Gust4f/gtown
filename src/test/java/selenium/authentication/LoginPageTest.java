package selenium.authentication;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class LoginPageTest extends StormpathPageTest {

    @BeforeClass
    public static void init() {
        ENV = "Desktop";
    }

    @Before
    public void setUp() {
        goTo(LOGIN);
    }

    @Test
    public void test_background_video() {
        assertThat(findFirst("#bgndVideo")).hasClass("mb_YTPlayer");
        assertThat(findFirst(".mbYTP_wrapper")).isDisplayed();
    }

    @Test
    public void test_flex_slider_rotation() {
        assertThat(findFirst(".one")).isDisplayed();
        assertThat(findFirst(".two")).isNotDisplayed();
        assertThat(findFirst(".three")).isNotDisplayed();
        assertThat(findFirst(".four")).isNotDisplayed();
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(".one")));
        assertThat(findFirst(".one")).isNotDisplayed();
        assertThat(findFirst(".two")).isDisplayed();
        assertThat(findFirst(".three")).isNotDisplayed();
        assertThat(findFirst(".four")).isNotDisplayed();
        webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(".two")));
        assertThat(findFirst(".one")).isNotDisplayed();
        assertThat(findFirst(".two")).isNotDisplayed();
        assertThat(findFirst(".three")).isDisplayed();
        assertThat(findFirst(".four")).isNotDisplayed();
        webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(".three")));
        assertThat(findFirst(".one")).isNotDisplayed();
        assertThat(findFirst(".two")).isNotDisplayed();
        assertThat(findFirst(".three")).isNotDisplayed();
        assertThat(findFirst(".four")).isDisplayed();
        webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(".four")));
        assertThat(findFirst(".one")).isDisplayed();
        assertThat(findFirst(".two")).isNotDisplayed();
        assertThat(findFirst(".three")).isNotDisplayed();
        assertThat(findFirst(".four")).isNotDisplayed();
    }

    @Test
    public void test_login_form_submission() {
        fill(".group-login input").with("Test");
        fill(".group-password input").with("Password1");
        submit(".login-form");
        assertThat(alert()).isPresent().hasText("Invalid username or password.");
    }

    @Test
    public void test_that_bad_credentials_alert_is_closable() {
        fill(".group-login input").with("Test");
        fill(".group-password input").with("Password1");
        submit(".login-form");
        assertThat(alert()).isPresent().hasText("Invalid username or password.");
        click(".close");
        assertThat(alert().getText()).isNotEqualTo("Invalid username or password.");
    }

    @Test
    public void test_facebook_login_botton_is_disabled() {
        assertThat(findFirst("#facebook")).hasClass("disabled");
        assertThat(findFirst("#facebook").getAttribute("disabled")).contains("true");
    }

    @Test
    public void test_forgot_password_link() {
        click("#forgot");
        assertThat(url()).isEqualTo(FORGOT);
    }

    @Test
    public void test_registration_link() {
        click("#register");
        assertThat(url()).isEqualTo(REGISTER);
    }
}
