package selenium.authentication;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class RegistrationPageTest extends StormpathPageTest {

    @BeforeClass
    public static void init() {
        ENV = "Desktop";
    }

    @Before
    public void setUp() {
        goTo(REGISTER);
    }

    @Test
    public void test_submit_form_with_too_short_password() {
        fill(".group-givenName input").with("Test");
        fill("group-surName").with("Test");
        fill("group-email").with("Test@Test.com");
        fill("test");
        assertThat(alert()).isPresent().hasText("Account password minimum length not satisfied.");
    }

    @Test
    public void test_submit_form_with_password_without_numerics() {
        fill(".group-givenName input").with("Test");
        fill("group-surName").with("Test");
        fill("group-email").with("Test@Test.com");
        fill("testtesttesttest");
        assertThat(alert()).isPresent().hasText("Password requires at least 1 numeric character.");
    }

    @Test
    public void test_submit_form_with_invalid_email() {
        fill(".group-givenName input").with("Test");
        fill("group-surName").with("Test");
        fill("group-email").with("Test");
        fill("G6Hty&%98");
        assertThat(alert()).isPresent().hasText("Account email address is in an invalid format.");
    }

    @Test
    public void test_go_back_to_login_page() {
        click(".to-login");
        assertThat(url()).isEqualTo(LOGIN);
    }
}
