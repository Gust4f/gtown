package me.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Variation of mkyong's tests @:
 * http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression
 */
public class EmailAddressValidatorTest {

    @Test
    public void testValidEmails() {
        String[] emails = {"mkyong@yahoo.com",
                "mkyong-100@yahoo.com", "mkyong.100@yahoo.com",
                "mkyong111@mkyong.com", "mkyong-100@mkyong.net",
                "mkyong.100@mkyong.com.au", "mkyong@1.com",
                "mkyong@gmail.com.com", "mkyong+100@gmail.com",
                "mkyong-100@yahoo-test.com"};
        for (String temp : emails) {
            boolean valid = EmailAddressValidator.isValidEmailAddress(temp);
            assertThat(valid).isTrue();
        }
    }

    @Test
    public void testInValidEmails() {
        String[] emails = {"mkyong", "mkyong@.com.my",
                "mkyong123@gmail.a", "mkyong123@.com", "mkyong123@.com.com",
                ".mkyong@mkyong.com", "mkyong()*@gmail.com", "mkyong@%*.com",
                "mkyong..2002@gmail.com", "mkyong.@gmail.com",
                "mkyong@mkyong@gmail.com", "mkyong@gmail.com.1a"};
        for (String temp : emails) {
            boolean valid = EmailAddressValidator.isValidEmailAddress(temp);
            assertThat(valid).isFalse();
        }
    }
}