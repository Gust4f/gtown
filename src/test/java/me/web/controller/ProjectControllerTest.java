package me.web.controller;

import me.service.AuthenticationCheckService;
import me.web.controller.helpers.Helper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProjectControllerTest {
    private final static MediaType TYPE = MediaType.parseMediaType("text/html;charset=UTF-8");

    @Mock
    private AuthenticationCheckService auth;

    @InjectMocks
    private ProjectController projectController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders
                .standaloneSetup(projectController)
                .setViewResolvers(Helper.getViewResolver())
                .build();
    }

    @Test
    public void testGetPage() throws Exception {
        mockMvc.perform(get("/projects").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "GTown Projects"))
                .andExpect(model().attribute("projects", "active"))
                .andExpect(view().name("projects"));
    }

    @Test
    public void testGetPageFragment() throws Exception {
        mockMvc.perform(get("/projects/getFragment").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(view().name("fragments/projectsfragment"));
    }
}