package me.web.controller;

import me.model.nodes.Picture;
import me.service.AuthenticationCheckService;
import me.service.PictureService;
import me.web.controller.helpers.Helper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import java.io.FileNotFoundException;
import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PictureControllerTest {

    private final static MediaType TYPE = MediaType.parseMediaType("text/html;charset=UTF-8");
    private final static String FILE_NAME = "input.png";

    @Mock
    private PictureService pictureService;

    @Mock
    private AuthenticationCheckService auth;

    @InjectMocks
    private PictureController pictureController;

    private MockMvc mockMvc;
    private Date date;
    private MockMultipartFile file;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        date = Calendar.getInstance().getTime();
        final byte[] bytes = new byte[20];
        new Random().nextBytes(bytes);
        file = new MockMultipartFile("file", FILE_NAME, "image/png", bytes);

        this.mockMvc = MockMvcBuilders
                .standaloneSetup(pictureController)
                .setViewResolvers(Helper.getViewResolver())
                .build();
        when(pictureService.getPictureCount()).thenReturn((long) 2);
        when(pictureService.getPictures()).thenReturn(getDummyPictures());
        when(pictureService.getPicture(anyString())).thenReturn(Optional.of(buildDummyPicture()));
    }

    private List<Picture> getDummyPictures() throws FileNotFoundException {
        final List<Picture> dummy = new ArrayList<>();
        dummy.add(buildDummyPicture());
        dummy.add(buildDummyPicture());
        return dummy;
    }

    private Picture buildDummyPicture() {
        final Picture picture = new Picture();
        picture.setDate(date);
        picture.setName("test");
        picture.setDescription("test");
        picture.setContentType("image/png");
        return picture;
    }

    @Test
    public void testGetPage() throws Exception {
        mockMvc.perform(get("/pictures").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "GTown Pictures"))
                .andExpect(model().attribute("pictures", "active"))
                .andExpect(model().attribute("currentPicture", 1))
                .andExpect(model().attribute("numberOfPictures", 2))
                .andExpect(model().attribute("header", date))
                .andExpect(model().attribute("data", "test"))
                .andExpect(model().attribute("description", "test"))
                .andExpect(view().name("pictures"));
    }

    @Test
    public void testGetPageFragment() throws Exception {
        mockMvc.perform(get("/pictures/getFragment").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("currentPicture", 1))
                .andExpect(model().attribute("numberOfPictures", 2))
                .andExpect(model().attribute("header", date))
                .andExpect(model().attribute("data", "test"))
                .andExpect(model().attribute("description", "test"))
                .andExpect(view().name("fragments/picturesfragment"));
    }

    @Test
    public void testGetPicturesPage() throws Exception {
        mockMvc.perform(get("/pictures/picture/1").accept(TYPE))
                .andExpect(model().attribute("title", "GTown Pictures"))
                .andExpect(model().attribute("pictures", "active"))
                .andExpect(model().attribute("currentPicture", 1))
                .andExpect(model().attribute("numberOfPictures", 2))
                .andExpect(model().attribute("header", date))
                .andExpect(model().attribute("data", "test"))
                .andExpect(model().attribute("description", "test"))
                .andExpect(view().name("pictures"));
    }

    @Test
    public void testGetPictureFragment() throws Exception {
        mockMvc.perform(get("/pictures/getPictureFragment/1").accept(TYPE))
                .andExpect(model().attribute("currentPicture", 1))
                .andExpect(model().attribute("numberOfPictures", 2))
                .andExpect(model().attribute("header", date))
                .andExpect(model().attribute("data", "test"))
                .andExpect(model().attribute("description", "test"))
                .andExpect(view().name("fragments/picturesfragment"));
    }

    @Test(expected = NestedServletException.class)
    public void testGetInvalidPicturePage() throws Exception {
        mockMvc.perform(get("/pictures/picture/100").accept(TYPE));
    }

    @Test
    public void testGetImage() throws Exception {
        mockMvc.perform(get("/pictures/image").param("name", "test"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testUploadPicture() throws Exception {
        when(pictureService.savePicture(any(), anyString(), anyString())).thenReturn(true);
        mockMvc.perform(fileUpload("/pictures/upload")
                .file(file)
                .param("description", "A test"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("uploaded", true))
                .andExpect(view().name("pictures"));
    }

    @Test
    public void testUploadEmptyPicture() throws Exception {
        file = new MockMultipartFile("file", FILE_NAME, "image/png", new byte[0]);
        when(pictureService.savePicture(any(), anyString(), anyString())).thenReturn(true);
        mockMvc.perform(fileUpload("/pictures/upload")
                .file(file)
                .param("description", "A test"))
                .andExpect(status().isOk())
                .andExpect(view().name("pictures"));
    }

    @Test
    public void testUploadPictureFail() throws Exception {
        when(pictureService.savePicture(any(), anyString(), any())).thenReturn(false);
        mockMvc.perform(fileUpload("/pictures/upload")
                .file(file)
                .param("description", "A test"))
                .andExpect(status().isOk())
                .andExpect(view().name("pictures"));
    }

//    @Test
//    public void testUploadPictureAsGuest() throws Exception {
//        when(pictureService.savePicture(any(), anyString(), any())).thenReturn(false);
//        mockMvc.perform(fileUpload("/pictures/upload")
//                .file(file)
//                .param("description", "A test"))
//                .andExpect(status().isFound())
//                .andExpect(view().name("redirect:/login"));
//    }
}