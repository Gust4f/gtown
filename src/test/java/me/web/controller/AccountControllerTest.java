package me.web.controller;

import com.stormpath.sdk.account.Account;
import me.service.AccountService;
import me.service.AuthenticationCheckService;
import me.util.URL;
import me.web.controller.helpers.DummyAccount;
import me.web.controller.helpers.Helper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AccountControllerTest {

    private final static MediaType TYPE = MediaType.parseMediaType("text/html;charset=UTF-8");

    @Mock
    private AccountService service;

    @Mock
    private AuthenticationCheckService auth;

    @InjectMocks
    private AccountController accountController;

    private MockMvc mockMvc;

    private Account dummy;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        dummy = DummyAccount.getDummy();

        when(auth.getAccount(any())).thenReturn(Optional.of(dummy));

        this.mockMvc = MockMvcBuilders
                .standaloneSetup(accountController)
                .setViewResolvers(Helper.getViewResolver())
                .build();
    }

    @Test
    public void testGetPage() throws Exception {
        this.mockMvc.perform(get("/account").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "Your Account"))
                .andExpect(model().attribute("account", dummy))
                .andExpect(view().name("account"));
    }

    @Test
    public void testGetPageFragment() throws Exception {
        this.mockMvc.perform(get("/account/getFragment").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("account", dummy))
                .andExpect(view().name("fragments/accountfragment"));
    }

    @Test
    public void testSuccessfulUpdateAccount() throws Exception {
        when(service.saveAccount(any(), anyString(), anyString(), anyString()
                , anyString(), anyString(), anyString(), anyString())).thenReturn(true);
        this.mockMvc.perform(post("/account/update")
                .param("givenName", "Test")
                .param("surname", "Test")
                .param("username", "Test")
                .param("email", "Test")
                .param("currentPassword", "Test")
                .param("password", "Test")
                .param("rpassword", "Test")
                .param("success", "Your account was updated!")
                .accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(view().name("account"));
    }

    @Test
    public void testFailedUpdateAccount() throws Exception {
        when(service.saveAccount(any(), anyString(), anyString(), anyString()
                , anyString(), anyString(), anyString(), anyString())).thenReturn(false);
        this.mockMvc.perform(post("/account/update")
                .param("givenName", "Test")
                .param("surname", "Test")
                .param("username", "Test")
                .param("email", "Test")
                .param("currentPassword", "Test")
                .param("password", "Test")
                .param("rpassword", "Test")
                .param("problem", "Your account could not be updated.")
                .accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(view().name("account"));
    }

    @Test
    public void testDeleteAccount() throws Exception {
        when(service.removeAccount(any(), any())).thenReturn(true);
        this.mockMvc.perform(post("/account/delete")
                .accept(TYPE)
                .param("password", "password"))
                .andExpect(status().isFound())
                .andExpect(view().name(URL.REDIRECT_TO_INDEX + "?status=accountDeleted"));
    }

    @Test
    public void testDeleteInvalidAccount() throws Exception {
        when(service.removeAccount(any(), any())).thenReturn(false);
        this.mockMvc.perform(post("/account/delete")
                .accept(TYPE)
                .param("password", "password"))
                .andExpect(status().isFound())
                .andExpect(view().name(URL.REDIRECT_TO_ACCOUNT.toString()));
    }
}