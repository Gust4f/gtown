package me.web.controller;

import me.model.nodes.Post;
import me.service.AuthenticationCheckService;
import me.service.PostService;
import me.web.controller.helpers.Helper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BlogControllerTest {

    private final static MediaType TYPE = MediaType.parseMediaType("text/html;charset=UTF-8");

    @Mock
    private PostService postService;
    @Mock
    private AuthenticationCheckService auth;

    @InjectMocks
    private BlogController blogController;

    private Post post1, post2;
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        post1 = new Post();
        post2 = new Post();
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(blogController)
                .setViewResolvers(Helper.getViewResolver())
                .build();
        when(postService.getPostCount()).thenReturn(2);
        when(postService.getPost(2)).thenReturn(Optional.of(post2));
        when(postService.getPost(1)).thenReturn(Optional.of(post1));
    }

    @Test
    public void testGetBlog() throws Exception {
        this.mockMvc.perform(get("/blog").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "GTown Blog"))
                .andExpect(model().attribute("blog", "active"))
                .andExpect(model().attribute("numberOfPosts", 2))
                .andExpect(model().attribute("currentPost", 2))
                .andExpect(model().attribute("post", post2))
                .andExpect(view().name("blog"));
    }

    @Test
    public void testGetBlogFragment() throws Exception {
        this.mockMvc.perform(get("/blog/getFragment").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("numberOfPosts", 2))
                .andExpect(model().attribute("currentPost", 2))
                .andExpect(model().attribute("post", post2))
                .andExpect(view().name("fragments/blogfragment"));
    }

    @Test
    public void testGetBlogById() throws Exception {
        this.mockMvc.perform(get("/blog/post/1").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "GTown Blog"))
                .andExpect(model().attribute("blog", "active"))
                .andExpect(model().attribute("numberOfPosts", 2))
                .andExpect(model().attribute("currentPost", 1))
                .andExpect(model().attribute("post", post1))
                .andExpect(view().name("blog"));
    }

    @Test(expected = NestedServletException.class)
    public void testGetBlogByInvalidId() throws Exception {
        this.mockMvc.perform(get("/blog/post/100").accept(TYPE));
    }

    @Test
    public void testGetBlogWithStatus() throws Exception {
        this.mockMvc.perform(get("/blog").param("status", "test").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "GTown Blog"))
                .andExpect(model().attribute("blog", "active"))
                .andExpect(model().attribute("numberOfPosts", 2))
                .andExpect(model().attribute("currentPost", 2))
                .andExpect(model().attribute("post", post2))
                .andExpect(model().attribute("status", "test"))
                .andExpect(view().name("blog"));
    }

    @Test
    public void testGetPostFragment() throws Exception {
        this.mockMvc.perform(get("/blog/getPostFragment/1")
                .accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("numberOfPosts", 2))
                .andExpect(model().attribute("currentPost", 1))
                .andExpect(model().attribute("post", post1))
                .andExpect(view().name("fragments/blogfragment"));

    }

    @Test
    public void testGetPostEditor() throws Exception {
        when(auth.getUsername(any())).thenReturn("Test");

        this.mockMvc.perform(get("/blog/addPost").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "Post Editor"))
                .andExpect(model().attribute("blog", "active"))
                .andExpect(view().name("addpost"));
    }

    @Test
    public void testAddPost() throws Exception {
        when(auth.getUsername(any())).thenReturn("Test");
        this.mockMvc.perform(post("/blog/addPost")
                .param("header", "HEAD")
                .param("body", "BODY")
                .accept(TYPE))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/blog"));
    }

    @Test
    public void testUpdatePost() throws Exception {
        when(auth.getUsername(any())).thenReturn("Test");
        this.mockMvc.perform(post("/blog/addPost")
                .param("postId", "1")
                .param("header", "HEAD")
                .param("body", "BODY")
                .accept(TYPE))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/blog"));
    }

    @Test
    public void testAddComment() throws Exception {
        when(auth.getUsername(any())).thenReturn("Test");
        this.mockMvc.perform(post("/blog/addComment")
                .param("post", "1")
                .param("postId", "1")
                .param("body", "BODY")
                .accept(TYPE))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/blog/post/1"));
    }

    @Test
    public void testAddCommentOnNonExistingPost() throws Exception {
        this.mockMvc.perform(post("/blog/addComment")
                .param("post", "100")
                .param("postId", "100")
                .param("body", "BODY")
                .accept(TYPE))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/blog"));
    }
}