package me.web.controller.helpers;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.account.AccountOptions;
import com.stormpath.sdk.account.AccountStatus;
import com.stormpath.sdk.account.EmailVerificationToken;
import com.stormpath.sdk.api.ApiKey;
import com.stormpath.sdk.api.ApiKeyCriteria;
import com.stormpath.sdk.api.ApiKeyList;
import com.stormpath.sdk.api.ApiKeyOptions;
import com.stormpath.sdk.application.ApplicationCriteria;
import com.stormpath.sdk.application.ApplicationList;
import com.stormpath.sdk.directory.CustomData;
import com.stormpath.sdk.directory.Directory;
import com.stormpath.sdk.group.*;
import com.stormpath.sdk.provider.ProviderData;
import com.stormpath.sdk.tenant.Tenant;

import java.util.Date;
import java.util.Map;

/**
 * ONLY FOR TESTING
 */
public final class DummyAccount {

    public DummyAccount(){}

    public final static Account getDummy() {
        return new Account() {
            @Override
            public Date getCreatedAt() {
                return null;
            }

            @Override
            public Date getModifiedAt() {
                return null;
            }

            @Override
            public String getUsername() {
                return "Test";
            }

            @Override
            public Account setUsername(String username) {
                return null;
            }

            @Override
            public String getEmail() {
                return "Test";
            }

            @Override
            public Account setEmail(String email) {
                return null;
            }

            @Override
            public Account setPassword(String password) {
                return null;
            }

            @Override
            public String getGivenName() {
                return "Test";
            }

            @Override
            public Account setGivenName(String givenName) {
                return null;
            }

            @Override
            public String getMiddleName() {
                return "Test";
            }

            @Override
            public Account setMiddleName(String middleName) {
                return null;
            }

            @Override
            public String getSurname() {
                return "Test";
            }

            @Override
            public Account setSurname(String surname) {
                return null;
            }

            @Override
            public String getFullName() {
                return "Test";
            }

            @Override
            public AccountStatus getStatus() {
                return null;
            }

            @Override
            public Account setStatus(AccountStatus status) {
                return null;
            }

            @Override
            public GroupList getGroups() {
                return null;
            }

            @Override
            public GroupList getGroups(Map<String, Object> queryParams) {
                return null;
            }

            @Override
            public GroupList getGroups(GroupCriteria criteria) {
                return null;
            }

            @Override
            public Directory getDirectory() {
                return null;
            }

            @Override
            public Tenant getTenant() {
                return null;
            }

            @Override
            public GroupMembershipList getGroupMemberships() {
                return null;
            }

            @Override
            public GroupMembership addGroup(Group group) {
                return null;
            }

            @Override
            public GroupMembership addGroup(String hrefOrName) {
                return null;
            }

            @Override
            public Account removeGroup(Group group) {
                return null;
            }

            @Override
            public Account removeGroup(String hrefOrName) {
                return null;
            }

            @Override
            public EmailVerificationToken getEmailVerificationToken() {
                return null;
            }

            @Override
            public Account saveWithResponseOptions(AccountOptions responseOptions) {
                return null;
            }

            @Override
            public boolean isMemberOfGroup(String hrefOrName) {
                return false;
            }

            @Override
            public ProviderData getProviderData() {
                return null;
            }

            @Override
            public ApiKeyList getApiKeys() {
                return null;
            }

            @Override
            public ApiKeyList getApiKeys(Map<String, Object> queryParams) {
                return null;
            }

            @Override
            public ApiKeyList getApiKeys(ApiKeyCriteria criteria) {
                return null;
            }

            @Override
            public ApiKey createApiKey() {
                return null;
            }

            @Override
            public ApiKey createApiKey(ApiKeyOptions options) {
                return null;
            }

            @Override
            public ApplicationList getApplications() {
                return null;
            }

            @Override
            public ApplicationList getApplications(Map<String, Object> queryParams) {
                return null;
            }

            @Override
            public ApplicationList getApplications(ApplicationCriteria criteria) {
                return null;
            }

            @Override
            public void delete() {

            }

            @Override
            public CustomData getCustomData() {
                return null;
            }

            @Override
            public String getHref() {
                return null;
            }

            @Override
            public void save() {

            }
        };
    }
}
