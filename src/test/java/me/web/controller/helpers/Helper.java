package me.web.controller.helpers;

import org.springframework.web.servlet.view.InternalResourceViewResolver;

public final class Helper {

    Helper(){}

    public final static InternalResourceViewResolver getViewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("classpath:/templates/");
        viewResolver.setSuffix(".jade");
        return viewResolver;
    }
}
