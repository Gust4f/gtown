package me.web.controller;

import me.service.AuthenticationCheckService;
import me.service.MailSendService;
import me.web.controller.helpers.Helper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ContactControllerTest {
    private final static MediaType TYPE = MediaType.parseMediaType("text/html;charset=UTF-8");

    @Mock
    private MailSendService mailSendService;

    @Mock
    private AuthenticationCheckService auth;

    @InjectMocks
    private ContactController contactController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders
                .standaloneSetup(contactController)
                .setViewResolvers(Helper.getViewResolver())
                .build();
        when(auth.getUsername(any())).thenReturn("Guest");
    }

    @Test
    public void testGetPage() throws Exception {
        this.mockMvc.perform(get("/contact").accept(TYPE))
                .andExpect(model().attribute("title", "GTown Contact"))
                .andExpect(model().attribute("contact", "active"))
                .andExpect(status().isOk())
                .andExpect(view().name("contact"));
    }

    @Test
    public void testGetPageFragment() throws Exception {
        this.mockMvc.perform(get("/contact/getFragment").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(view().name("fragments/contactfragment"));
    }

    @Test
    public void testContactFormSubmit() throws Exception {
        when(mailSendService.sendMessage(anyString(), anyString(), anyString(), anyString())).thenReturn(true);

        this.mockMvc.perform(post("/contact/send")
                .param("name", "Test")
                .param("email", "Test")
                .param("check", "Test")
                .param("body", "Test")
                .accept(TYPE))
                .andExpect(model().attribute("title", "GTown Contact"))
                .andExpect(model().attribute("contact", "active"))
                .andExpect(model().attribute("success", true))
                .andExpect(status().isOk())
                .andExpect(view().name("contact"));
    }

    @Test
    public void testContactFormSubmitFailure() throws Exception {
        when(mailSendService.sendMessage(anyString(), anyString(), anyString(), anyString())).thenReturn(false);

        this.mockMvc.perform(post("/contact/send")
                .param("name", "Test")
                .param("email", "Test")
                .param("check", "Test")
                .param("body", "Test")
                .accept(TYPE))
                .andExpect(model().attribute("title", "GTown Contact"))
                .andExpect(model().attribute("contact", "active"))
                .andExpect(model().attribute("fail", true))
                .andExpect(status().isOk())
                .andExpect(view().name("contact"));
    }
}