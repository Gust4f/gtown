package me.web.controller;

import me.model.nodes.Post;
import me.service.AuthenticationCheckService;
import me.service.PostService;
import me.web.controller.helpers.Helper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class IndexControllerTest {

    private final static MediaType TYPE = MediaType.parseMediaType("text/html;charset=UTF-8");

    @Mock
    private PostService postService;

    @Mock
    private AuthenticationCheckService auth;

    @InjectMocks
    private IndexController indexController;

    private Post post;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        post = new Post();

        this.mockMvc = MockMvcBuilders
                .standaloneSetup(indexController)
                .setViewResolvers(Helper.getViewResolver())
                .build();

        when(postService.getPostCount()).thenReturn(1);
        when(postService.getPost(1)).thenReturn(Optional.of(post));
    }

    @Test
    public void testGetPage() throws Exception {
        this.mockMvc.perform(get("/").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "GTown"))
                .andExpect(model().attribute("home", "active"))
                .andExpect(model().attribute("currentPost", 1))
                .andExpect(model().attribute("post", post))
                .andExpect(view().name("index"));
    }

    @Test
    public void testGetPageFragment() throws Exception {
        this.mockMvc.perform(get("/index/getFragment").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("currentPost", 1))
                .andExpect(model().attribute("post", post))
                .andExpect(view().name("fragments/indexfragment"));
    }

    @Test
    public void testGetIndex() throws Exception {
        this.mockMvc.perform(get("/index").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "GTown"))
                .andExpect(model().attribute("home", "active"))
                .andExpect(model().attribute("currentPost", 1))
                .andExpect(model().attribute("post", post))
                .andExpect(view().name("index"));
    }

    @Test
    public void testGetIndexWithStatus() throws Exception {
        this.mockMvc.perform(get("/index?status=accountDeleted").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "GTown"))
                .andExpect(model().attribute("home", "active"))
                .andExpect(model().attribute("currentPost", 1))
                .andExpect(model().attribute("post", post))
                .andExpect(model().attribute("accountDeleted", true))
                .andExpect(view().name("index"));
    }
}