package me.web.controller;

import me.model.nodes.Post;
import me.service.PostService;
import me.web.view.RSSViewer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RSSControllerTest {

    @Mock
    private PostService postService;

    @Mock
    private RSSViewer rssViewer;

    @InjectMocks
    private RSSController rssController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders
                .standaloneSetup(rssController)
                .build();
    }

    @Test
    public void testGetFeedInRssWithNoPosts() throws Exception {
        when(postService.getPostCount()).thenReturn(0);

        mockMvc.perform(get("/rss").accept(MediaType.APPLICATION_ATOM_XML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("feedContent", Collections.emptyList()));
    }

    @Test
    public void testGetFeedInRssWithOnePost() throws Exception {
        final List<Post> posts = new ArrayList<>();
        posts.add(new Post());
        when(postService.getPostCount()).thenReturn(1);
        when(postService.getPost(1)).thenReturn(Optional.of(posts.get(0)));

        mockMvc.perform(get("/rss").accept(MediaType.APPLICATION_ATOM_XML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("feedContent", posts));
    }
}