package me.web.controller;

import me.service.ReportService;
import me.web.controller.helpers.Helper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AboutControllerTest {
    private final static MediaType TYPE = MediaType.parseMediaType("text/html;charset=UTF-8");

    @Mock
    private ReportService reportService;

    @InjectMocks
    private AboutController aboutController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders
                .standaloneSetup(aboutController)
                .setViewResolvers(Helper.getViewResolver())
                .build();

        when(reportService.getReportMap()).thenReturn(Collections.emptyMap());
    }

    @Test
    public void testGetPage() throws Exception {
        mockMvc.perform(get("/about").accept(TYPE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("title", "GTown About"))
                .andExpect(model().attribute("about", "active"))
                .andExpect(model().attribute("craptop_status", "online"))
                .andExpect(model().attribute("stormpath_status", "online"))
                .andExpect(model().attribute("mongodb_status", "online"))
                .andExpect(model().attribute("redis_status", "online"))
                .andExpect(model().attribute("email_status", "offline"))
                .andExpect(model().attribute("owncloud_status", "online"))
                .andExpect(model().attribute("ut_status", "offline"))
                .andExpect(model().attribute("tox_status", "online"))
                .andExpect(view().name("about"));
    }

    @Test
    public void testGetPageFragment() throws Exception {
        mockMvc.perform(get("/about/getFragment").accept(TYPE))
                .andExpect(model().attribute("craptop_status", "online"))
                .andExpect(model().attribute("stormpath_status", "online"))
                .andExpect(model().attribute("mongodb_status", "online"))
                .andExpect(model().attribute("redis_status", "online"))
                .andExpect(model().attribute("email_status", "offline"))
                .andExpect(model().attribute("owncloud_status", "online"))
                .andExpect(model().attribute("ut_status", "offline"))
                .andExpect(model().attribute("tox_status", "online"))
                .andExpect(status().isOk())
                .andExpect(view().name("fragments/aboutfragment"));
    }

    @Test
    public void testGetMetricsFragment() throws Exception {
        mockMvc.perform(get("/about/getMetricsFragment").accept(TYPE))
                .andExpect(model().attribute("craptop_status", "online"))
                .andExpect(model().attribute("stormpath_status", "online"))
                .andExpect(model().attribute("mongodb_status", "online"))
                .andExpect(model().attribute("redis_status", "online"))
                .andExpect(model().attribute("email_status", "offline"))
                .andExpect(model().attribute("owncloud_status", "online"))
                .andExpect(model().attribute("ut_status", "offline"))
                .andExpect(model().attribute("tox_status", "online"))
                .andExpect(view().name("fragments/metricsfragment"));
    }
}