package me.service;

import me.dao.neo4j.GustafsNeo4jOperations;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

public class PictureServiceTest {

    private final static String FILE_NAME = "test";
    private final static String FULL_NAME = FILE_NAME + ".png";
    private final static String USER_NAME = "tester";
    private final static String DESCRIPTION = "this is a test";

    @Mock
    private GustafsNeo4jOperations neo4jOperations;

    @InjectMocks
    private PictureService pictureService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSavePicture() throws Exception {
        final MultipartFile file = new MockMultipartFile(FILE_NAME, FULL_NAME, "image/png", new byte[0]);

        when(neo4jOperations.save(any())).thenReturn(Optional.empty());

        final boolean result = pictureService.savePicture(file, DESCRIPTION, USER_NAME);
        assertThat(result).isTrue();
    }

    @Test
    public void testSavePictureException() throws Exception {
        final MultipartFile file = new MockMultipartFile(FILE_NAME, "test.png", "image/png", new byte[0]);

        when(neo4jOperations.save(any())).thenThrow(new IOException("Problem"));

        final boolean result = pictureService.savePicture(file, DESCRIPTION, USER_NAME);
        assertThat(result).isFalse();
    }
}