Feature:
  As an unauthenticated user I should not have an account and
  log out menu item

  Scenario: Unauthenticated users should not see log out menu item
    Given the user is unauthenticated on the index page
    Then the log out menu item is not displayed

  Scenario: Unauthenticated users should not see account menu item
    Given the user is unauthenticated on the index page
    Then the account menu item is not displayed