Feature:
  As a user I want to be able to subscribe to posts
  with a rss feed.

  Scenario: User clicks rss feed
    Given the user is viewing a blog post
    When the user clicks on the rss image
    Then the user is redirected to the rss feed