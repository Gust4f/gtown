Feature:
  As a user I want to be able to quickly navigate
  from the element on the index page.

  Scenario: More details from welcoming jumbotron
    Given the user is on the index page
    When the user clicks on learn more
    Then the user is directed to the about page

