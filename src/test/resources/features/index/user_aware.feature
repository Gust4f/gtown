Feature:
  As a user I want to be treated differently if I
  am authenticated and if I am  just a guest.

  Scenario: Guest user visits index page
    Given the user is unauthenticated on the index page
    Then the user should be greeted as a guest

  Scenario: An authenticated user visits index page
    Given the user is authenticated on the index page
    Then the user should be greeted as the user and see additional info

