Feature:
  As an authenticated user I have an account and
  log out menu item

  Scenario: Authenticated users should see log out menu item
    Given the user is authenticated on the index page
    Then the log out menu item is displayed

  Scenario: Authenticated users should see account menu item
    Given the user is authenticated on the index page
    Then the account menu item is displayed