Feature:
  As a registered user I want to be able to log in
  to my account and gain write access.

  Scenario: Attempt to log in with valid credentials
    Given the user is on the login page
    When the user enters username
    And the user enters password
    And clicking submit login form
    Then the index page is shown as an authenticated user

  Scenario: Attempt to log in with invalid password
    Given the user is on the login page
    When the user enters username
    And the user enters the wrong password
    And clicking submit login form
    Then the page is reloaded with an error alert

  Scenario: Attempt to log in with non existing account
    Given the user is on the login page
    When the user enters an unregistered username
    And the user enters password
    And clicking submit login form
    Then the page is reloaded with an error alert

