Feature:
  As an authenticated user I want to be able to
  update the information associated with my account.

  Scenario: Updating account with new valid information
    Given the user is on the account page
    When the user enters first name
    And the user enters last name
    And the user enters username
    And the user enters new password
    And the user repeats the new password
    And the user enters the old password
    And clicking submit updates
    Then the page is reloaded and a success alert is displayed

  Scenario: Attempting to update account with valid information but wrong password
    Given the user is on the account page
    When the user enters first name
    And the user enters last name
    And the user enters username
    And the user enters new password
    And the user repeats the new password
    And the user enters the wrong password
    And clicking submit updates
    Then the page is reloaded and an error alert is displayed

  Scenario: Attempting to update account with occupied username
    Given the user is on the account page
    When the user enters first name
    And the user enters last name
    And the user enters an occupied username
    And the user enters new password
    And the user repeats the new password
    And the user enters the old password
    And clicking submit updates
    Then the page is reloaded and an error alert is displayed

  Scenario: Attempting to update account with unmatching new password
    Given the user is on the account page
    When the user enters first name
    And the user enters last name
    And the user enters an occupied username
    And the user enters new password
    And the user repeats wrong password
    And the user enters the old password
    And clicking submit updates
    Then the page is reloaded and an error alert is displayed