Feature:
  As a user having registered an account I want
  to be able to follow the confirmation email and
  active my account.

  Scenario: Follow confirmation email to complete account registration
    Given the user is reading email
    When the user clicks confirm account registration
    Then the user is directed to the index page with a success alert
