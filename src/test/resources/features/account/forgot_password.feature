Feature:
  As a registered user I want to be able to
  send myself a password reset email

  Scenario: Attempting to reset password of valid account
    Given the user is on the forgot password page
    When the user enters email
    And clicking submit forgot form
    Then the user is directed to the index page with a success alert

  Scenario: Attempting to reset password of non-existent account
    Given the user is on the forgot password page
    When the user enters an unregistered email
    And clicking submit forgot form
    Then the user is shown an error alert