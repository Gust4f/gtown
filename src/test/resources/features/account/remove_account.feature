Feature:
  As an authenticated user I want to be able to
  remove my account and all linked content I have
  contributed with in the process.

  Scenario: Attempt to remove account
    Given the user is on the account page
    When clicking remove account
    Then the user is redirected to index with a success alert and is no longer authenticated