Feature:
  As a registered user I want to be able to reset my password

  Scenario: Attempting to reset password with valid link
    Given the user is reading email
    When the user clicks on reset password link
    And the user enters new password
    And the user repeats the new password
    And clicking submit reset form
    Then the user is directed to the index page with a success alert

  Scenario: Attempting to reset password with invalid link
    Given the user is reading email
    When the user clicks on reset password link
    Then the user is directed to the index page with an error alert