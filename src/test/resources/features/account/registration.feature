Feature:
  As a user I want to be able to register for
  an account to gain write access.

  Scenario: Attempt a valid account registration
    Given the user is on the registration page
    When the user enters first name
    And the user enters last name
    And the user enters email
    And the user enters username
    And the user enters password
    And clicking submit registration form
    Then a new account is created with a success alert