Feature:
  As a user with javascript enabled I expect to be
  navigating using the HTML5 history API to avoid page
  reloads while still getting complete navigation history

  Scenario: Go to blog page without browser reloading
    Given the user is on the index page
    When the blog menu item is clicked
    Then the user is shown the blog page without the browser reloading

  Scenario: Go back in history
    Given the user is on the index page
    When the blog menu item is clicked
    And the user clicks back/previous
    Then the user is back at the index page

  Scenario: Go forward in history
    Given the user has moved from the index to the blog page
    When the user clicks back/previous
    And the user clicks forward/next
    Then the user is back at the blog page

  Scenario: Go to blog using mobile menu
    Given the user is on the index page
    When the blog menu item is clicked
    Then the mobile menu is collapsed and the blog page is shown