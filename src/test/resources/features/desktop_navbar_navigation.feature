Feature:
  As a user I want to be able to navigate using
  the desktop sized navbar

  Scenario: Go to projects page when clicking the projects menu item
    Given the user is on the index page
    When the projects menu item is clicked
    Then the user is shown the projects page

  Scenario: Go to pictures page when clicking on the pictures menu item
    Given the user is on the index page
    When the pictures menu item is clicked
    Then the user is shown the pictures page

  Scenario: Go to blog page when clicking on the blog menu item
    Given the user is on the index page
    When the blog menu item is clicked
    Then the user is shown the blog page

  Scenario: Go to about page when clicking on the about menu item
    Given the user is on the index page
    When the about menu item is clicked
    Then the user is shown the about page

  Scenario: Go to contact page when clicking on the contact menu item
    Given the user is on the index page
    When the contact menu item is clicked
    Then the user is shown the contact page

  Scenario: Go to login page when clicking on the login menu item
    Given the user is on the index page
    When the login menu item is clicked
    Then the user is shown the login page