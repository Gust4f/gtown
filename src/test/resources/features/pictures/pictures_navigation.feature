Feature:
  As a user I want to be able to navigate a
  picture gallery.

  Scenario: Get next picture
    Given the user is on the pictures page
    When the user clicks the next button
    Then the current picture scrolls out and a new appear

  Scenario: Get previous picture
    Given the user is on the picture page
    When the user clicks the previous button
    Then the current picture scrolls out and a new appear

  Scenario: Newest picture does not have a previous arrow
    Given the user is on the pictures page
    When the user is on the newest picture
    Then there is no previous picture element displayed

  Scenario: Oldest picture does not have a next arrow
    Given the user is on the pictures page
    When the user is on the oldest picture
    Then there is no next picture element displayed