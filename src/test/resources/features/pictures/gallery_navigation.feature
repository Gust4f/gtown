Feature:
  As a user I want to be able to go to the
  different galleries available

  Scenario: Unauthenticated users are directed to login
    Given the user is unauthenticated on the pictures page
    When the user clicks on a gallery
    Then the user is redirected to the login page

  Scenario: Authenticated users can browse full galleries
    Given the user is authenticated on the pictures page
    When the user clicks on a gallery
    Then a gallery dialog with images is displayed