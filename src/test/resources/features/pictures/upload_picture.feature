Feature:
  As an administration user with write access I
  want to be able to upload a picture to the gallery

  Scenario: Uploading a picture
    Given the admin user is on the pictures page
    When the user clicks select image
    And selects an image in the dialog
    And writes a description
    And clicks upload
    Then the user is redirected to the newly uploaded picture

  Scenario: Invalid image type
    Given the admin user is on the pictures page
    When the user selects an invalid file type
    Then an error alert is displayed
