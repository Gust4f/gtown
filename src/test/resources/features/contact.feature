Feature:
  As a user I want to be able to send the creator of the
  site an email explaining how awesome the site is

  Scenario: Attempting to contact creator with valid data
    Given the user is on the contact page
    When the user enters name
    And the user enters email
    And the user enters a message
    And clicking submit contact form
    Then the page reloads and a success dialog is shown

  Scenario: Attempting to contact creator with invalid email
    Given the user is on the contact page
    When the user enters name
    And the user enters an invalid email
    And the user enters a message
    And clicking submit contact form
    Then the page reloads and an error dialog is shown

  Scenario: Attempting to contact creator with empty message
    Given the user is on the contact page
    When the user enters name
    And the user enters email
    And clicking submit contact form
    Then the page reloads and an error dialog is shown