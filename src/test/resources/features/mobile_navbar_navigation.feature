Feature:
  As a user I want to be able to navigate using
  the collapsed mobile sized navbar

  Scenario: Go to projects page when clicking the projects menu item
    Given the user is on the mobile index page
    When the user opens the mobile menu
    And the projects menu item is clicked
    Then the user is shown the projects page

  Scenario: Go to pictures page when clicking on the pictures menu item
    Given the user is on the mobile index page
    When the user opens the mobile menu
    And the pictures menu item is clicked
    Then the user is shown the pictures page

  Scenario: Go to blog page when clicking on the blog menu item
    Given the user is on the mobile index page
    When the user opens the mobile menu
    And the blog menu item is clicked
    Then the user is shown the blog page

  Scenario: Go to about page when clicking on the about menu item
    Given the user is on the mobile index page
    When the user opens the mobile menu
    And the about menu item is clicked
    Then the user is shown the about page

  Scenario: Go to contact page when clicking on the contact menu item
    Given the user is on the mobile index page
    When the user opens the mobile menu
    And the contact menu item is clicked
    Then the user is shown the contact page

  Scenario: Go to login page when clicking on the login menu item
    Given the user is on the mobile index page
    When the user opens the mobile menu
    And the login menu item is clicked
    Then the user is shown the login page