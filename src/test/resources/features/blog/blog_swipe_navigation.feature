Feature:
  As a user of a device with a touch screen I want
  to be able to swipe between blog posts.

  Scenario: Swipes left
   Given the user is on the blog page
    When the user swipes left
    Then the previous blog post is displayed

  Scenario: Swipes right
    Given the user is on the blog page
    When the user swipes right
    Then the next blog post is displayed