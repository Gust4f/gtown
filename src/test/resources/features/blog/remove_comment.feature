Feature:
  As an authenticated user I want to be able to
  remove a comment I have made on a post

  Scenario: Attempting to remove a comment
    Given the user is authenticated on the blog page
    When clicking delete comment on a comment the user wrote
    Then the page is reloaded with a success alert


