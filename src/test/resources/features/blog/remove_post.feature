Feature:
  As an authenticated admin user I want to be able
  to remove a blog post I have written

  Scenario: Attempt to remove a blog post
    Given the user is authenticated on the blog page
    When clicking delete post
    Then the user is directed to blog with a success alert

