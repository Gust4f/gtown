Feature:
  As an authenticated admin user I want to be able
  to write blog posts

  Scenario: Attempt to write blog post
    Given the user is authenticated on the blog page
    When clicking create post
    Then the create post form is shown
    When the user enters a header
    And the user enters a post body
    And clicking submit blog post
    Then the user is directed to the new post with a success alert

