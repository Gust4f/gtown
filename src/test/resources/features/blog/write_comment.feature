Feature:
  As an authenticated user I want to be able to
  write a comment on a post

  Scenario: Attempting to write a valid comment
    Given the user is authenticated on the blog page
    When the user enters comment
    And clicking submit comment form
    Then the page is reloaded and a new comment is added

  Scenario: Attempting to write an empty comment
    Given the user is authenticated on the blog page
    And clicking submit comment form
    Then the page is reloaded and an error alert is shown