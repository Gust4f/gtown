Feature:
  As a user I want to be able to navigate through the
  blog posts using next and previous buttons

  Scenario: Get next post
    Given the user is on the blog page
    When the user clicks on the next
    Then the next post is displayed

  Scenario: Get previous post
    Given the user is on the blog page
    When the user clicks on the previous
    Then the previous post is displayed