describe('Sammy Navigation Module', function() {

    var links = {
        index : '<a id="index" href="/index">Index</a>',
        projects: '<a id="projects" href="/projects">Projects</a>',
        pictures:  '<a id="pictures" href="/pictures">Pictures</a>',
        blog: '<a id="blog" href="/blog">Blog</a>',
        about: '<a id="about" href="/about">About</a>',
        contact: '<a id="contact" href="/contact">Contact</a>'
    };

    var loadFixtures = function loadFixtures(pate, html) {
        $(html).appendTo('#wrapper');
    };

    beforeEach(function() {
        $('<div id="wrapper"></div>').appendTo('body');
        $.each(links, loadFixtures);
        nav.run();

        jasmine.Ajax.install();
    });

    afterEach(function() {
        jasmine.Ajax.uninstall();
    });

    var runRouteSpec = function(link, content) {
        it('click on ' + link, function () {
            $('#' + link).trigger('click');

            jasmine.Ajax.requests.mostRecent().respondWith({
                status: 200,
                statusText: 'HTTP/1.1 200 OK',
                contentType: 'text/xml;charset=UTF-8',
                responseText: 'Fetched and updated'
            });

            console.log('wrapper: ' + $('#wrapper').html());
            expect($('#wrapper').html()).toEqual('Fetched and updated');
        });
    };

    $.each(links, runRouteSpec);
});