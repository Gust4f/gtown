# GTown

O.B.S: We are now live in the cloud. Hosted by Amazon WS with 1 core on 2.5Ghz and 1GB of primary memory and low network availablity (free tier). May be slow.

This is the source code for gtown. There are screenshots in the folder, generated from the selenium integration tests so note that PhantomJS cannot render everything perfectly for the screenshots.

## Data Model

![Data model](data-model.jpg "Data model")

### Manually building data model in the neo4j browser

![Neo4j](neo4j.png "Neo4j")

#### Creating the three nodes that a presentable post consists of

```
CREATE (user:User {username: 'Gustaf'})
```

```
CREATE (post:Post {body: 'test post', header: 'Test Header'})
```

```
CREATE (comment:Comment {body: 'test comment'})
```

#### Creating a relationship binding Gustaf to all the created posts which in this case is just the first we created.

```
MATCH (user:User), (post:Post)
WHERE user.username = 'Gustaf' AND post.header = 'Test Header'
CREATE (user)-[r:WROTE]->(post)
RETURN r
```

#### Creating a relationship biding the comment to the post.

```
MATCH (post:Post), (comment:Comment)
WHERE post.header = 'Test Header' AND comment.body = 'test comment'
CREATE (comment)-[r:WRITTEN_ON]->(post)
return r
```

#### Creating a relationship biding the comment to Gustaf as the author.

```
MATCH (user:User), (comment:Comment)
WHERE user.username = 'Gustaf' AND comment.body = 'test comment'
CREATE (user)-[r:WROTE]->(comment)
RETURN r
```

### Manually quering the model in the neo4j browser with cypher

Quering is made using Cypher. Cypher tries to resemble the syntax of SQL. One thing I was unsure of when I read the documentation was if the placement of the start and end node mattered in Cypher queries. In theory it shouldn't so I made a little test to see that it worked as expected in practice.

```
MATCH (user:User {username: 'Gustaf'})-[:WROTE]->(comment:Comment)
RETURN comment
```

really does equal

```
MATCH (comment:Comment)<-[:WROTE]-(user:User {username: 'Gustaf'})
RETURN comment
```

So it works just like in theory!

#### Custom queries 

```
MATCH (post:Post) 
WITH post 
ORDER BY post.id DESC 
MATCH p=(:User)-[:WROTE]->(post)-[:HAS_COMMENT]->(:Comment)<-[:WROTE]-(:User) 
RETURN p
```

```
MATCH (n:`Post`) 
WITH n 
ORDER BY n.id DESC 
MATCH p=(q)-[*0..1]->(n)-[*0..1]->(m)<-[*0..1]-(r) 
RETURN p
```

## Setup

* Install Gradle
* Install npm
* Execute perpare.sh

## Integration tests

The embedded MongoDB and Neo4j is removed. To run the integration suite you now have to start a disposable mongoDB and Neo4j instance yourself.

Example mongo:

```
./mongod --port 27000 --dbpath /tmp/mongo
```

Example Neo4j config (neo4j-server.properties):

* org.neo4j.server.database.location=/tmp/graph.db
* org.neo4j.server.webserver.port=6666
* dbms.security.auth_enabled=false

### Dependencies

* Selenium
* FluentLenium
* PhantomJS (Vm option: -Dphantomjs.binary.path=/usr/bin/phantomjs)

## Fonts Used:

* Shadows Into Light
* Corben
* Nobile
* League

## HTTP/2 SPDY4

The application now uses undertow and HTTP/2 also referred to SPDY4. It
is not supported in JAVA 8 so we need to give the JDK some love.

export JAVA_OPTS=-Xbootclasspath/p:$LOCATION/alpn-boot-8.1.5.v20150921.jar

## Docker

Docker images are bare minimum linux vm blueprints that are cross platform and easy to deploy
into runnable containers that can be securely interlinked with other docker containers without
getting access to the host operating system. It is possible to create custom bridged networks
and also link specific directories from the host as arguments upon container creation.

* docker pull mongo
* docker pull neo4j
* docker pull redis
* docker pull jenkins

### Production environment

Originally this application was hosted on a local machine. We used an embedded Derby SQL service at first,
then we converted to MongoDB. After some time we were introduced to Neo4j and made a hybrid using MongoDB
and Neo4j but now we only use Neo4j as our main database. We have also left the local hosting due to fan noise
etc that comes with hosting locally. On the local host we had multiple cores and many GB of primary memory as 
well as multiple TB of main storage of both SSD and HDD. We had resources in abundance. Now the application lives
in the cloud. More precisely the Amazon web services. We are currently only using the free tier which means that
the application now runs on 1 2.5Ghz core with 1GB of memory. That is quite a difference there. However, it actually
runs just fine which is kinda cool.

The cloud attempt was with Heroku which is a layer on top of the Amazon web services. However, I didn't find what I
wanted from that and hence we moved on to Tutum. Which is also kind of a layer on top of AWS but it is more a service
where you bring your own resources and they provide a remote management of those resources ignoring the platform below.
This worked but added an overhead I didn't think was necessary. I had to open many ports from EC2 to give Tutum the access
to control the docker system remotely. Tutum also uses a Ubuntu installation by default which I didn't like because I want
to use Red hat! So Tutum was eventually also dropped and now we currently have a Red hat instance set up where I directly
deploy the docker images just as we did locally on the local machine and that works fine and I only have the ports open that
I think I need. 

However, there is another step to take. This is currently not the most optimal solution. Amazon WS has a container service
which will replicate and load balance containers automatically. This however, I haven't explored yet and I didn't find the
management very intuitive so until I have read up on how it works and how I access private repos with images I am hosting
in the Red hat instance and then the below will still work since it is just a normal docker deployment atm.

### [Startup Neo4j](https://hub.docker.com/_/neo4j/)

I will have my database on the host operating system away from the actual neo4j container and link it into the container to be able to use the same database during development or local deploys as well as making it follow along with the automated backup.

* docker run -p 7474:7474 --name neo4j -v /neo4j:/data --env=NEO4J_AUTH=none -d neo4j

### [Startup MongoDB](https://hub.docker.com/_/mongo/)

I will have my database on the host operating system away from the actual MongoDB container and link it into the container to be able to use the same database during development or local deploys as well as making it follow along with the automated backup.

* docker run -p 27017:27017 --name mongodb -v /mongo/data/db:/data/db -d mongo

To test MongoDB instance in the container from a mongo shell:

* docker run -it --link mongodb:mongo --rm mongo sh -c 'exec mongo "$MONGO_PORT_27017_TCP_ADDR:$MONGO_PORT_27017_TCP_PORT/test"'

### [Starup Jenikns](https://hub.docker.com/_/jenkins/)

* docker run -p 9090:8080 -v jenkins:/var/jenkins_home jenkins

### [Startup Redis](https://hub.docker.com/_/redis/)

Redis will be used to cache stuff. Perhaps even session in the future.

* docker run -p 6379:6379 --name redis -d redis

* docker run -v /etc/redis.conf:/etc/redis.conf --name redis -d redis redis-server /etc/redis.conf

To test Redis instance in container:

* docker run -it --link redis:redis --rm redis sh -c 'exec redis-cli -h "$REDIS_PORT_6379_TCP_ADDR" -p "$REDIS_PORT_6379_TCP_PORT"'

#### Stormpath and Redis

```javascript
var cacheOptions = {
  store: 'redis',
  client: { /* optional - pass your own Redis client instance */},
  connection: {
    host: 'localhost',
    port: 6379
  },
  options: {
    return_buffers: false
  },
  ttl: 300,
  tti: 300
};

var client = new stormpath.Client({
  apiKey: apiKey,
  cacheOptions: cacheOptions
});
```
### Start main application and link to the MongoDB container.

* docker run -p 172.31.18.8:80:8080 -p 172.31.18.8:443:8443 -v /usr/share/GeoIP/:/usr/share/GeoIP/ --name gtown --link neo4j:neo4j -t gustaf/gtown:latest

## LOGGING

logging.level.org.springframework.security = DEBUG

logging.level.io.undertow.request = TRACE
