#!/bin/bash

command -v npm >/dev/null 2>&1 || { echo >&2 "Npm required but it's not installed.  Aborting."; exit 1; }

npm init -y
npm install less
sudo npm install -g grunt-cli
npm install grunt grunt-contrib-uglify grunt-contrib-jshint grunt-contrib-jasmine grunt-template-jasmine-istanbul --save
npm install grunt-contrib-watch grunt-contrib-less grunt-contrib-sass grunt-contrib-copy grunt-contrib-clean --save
npm install --save-dev jasmine-ajax
npm install bootstrap jquery fancybox sammy bootstrap-datepicker flexslider jquery.mb.ytplayer --save

sudo dnf install ruby
gem install sass

grunt snapshot

ALPN_VERSION=8.1.5.v20150921

wget http://central.maven.org/maven2/org/mortbay/jetty/alpn/alpn-boot/$ALPN_VERSION/alpn-boot-$ALPN_VERSION.jar

echo "All done!"
